<?php

/* CvutFitBiWT1BlogUiBundle:Post:index.html.twig */
class __TwigTemplate_4eba6b74a4ac1459b3946755e1cc865719829ff509fe660f3655bca9b33720f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig", "CvutFitBiWT1BlogUiBundle:Post:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_865616565464445e051b53fadd4e0da792dd18f7152825d7d82e69d39f72ef56 = $this->env->getExtension("native_profiler");
        $__internal_865616565464445e051b53fadd4e0da792dd18f7152825d7d82e69d39f72ef56->enter($__internal_865616565464445e051b53fadd4e0da792dd18f7152825d7d82e69d39f72ef56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CvutFitBiWT1BlogUiBundle:Post:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_865616565464445e051b53fadd4e0da792dd18f7152825d7d82e69d39f72ef56->leave($__internal_865616565464445e051b53fadd4e0da792dd18f7152825d7d82e69d39f72ef56_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_347ab338ee252aa4d1e5bf62596d9aecb48264033e535760c84faf27976e96ff = $this->env->getExtension("native_profiler");
        $__internal_347ab338ee252aa4d1e5bf62596d9aecb48264033e535760c84faf27976e96ff->enter($__internal_347ab338ee252aa4d1e5bf62596d9aecb48264033e535760c84faf27976e96ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Index";
        
        $__internal_347ab338ee252aa4d1e5bf62596d9aecb48264033e535760c84faf27976e96ff->leave($__internal_347ab338ee252aa4d1e5bf62596d9aecb48264033e535760c84faf27976e96ff_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3adab021db27b5b98be7af41461a93137e058728cf000e10dbf84afd53a62349 = $this->env->getExtension("native_profiler");
        $__internal_3adab021db27b5b98be7af41461a93137e058728cf000e10dbf84afd53a62349->enter($__internal_3adab021db27b5b98be7af41461a93137e058728cf000e10dbf84afd53a62349_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"categories\">

            ";
        // line 6
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "d605aa2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_d605aa2_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/d605aa2_trending2_1.jpg");
            // line 7
            echo "     <a href=\"";
            echo $this->env->getExtension('routing')->getPath("trending");
            echo "\">
        <img class=\"item\" src=\"";
            // line 8
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Trending\" />
    </a>
    ";
        } else {
            // asset "d605aa2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_d605aa2") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/d605aa2.jpg");
            // line 7
            echo "     <a href=\"";
            echo $this->env->getExtension('routing')->getPath("trending");
            echo "\">
        <img class=\"item\" src=\"";
            // line 8
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Trending\" />
    </a>
    ";
        }
        unset($context["asset_url"]);
        // line 11
        echo "
        ";
        // line 12
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a15f5fe_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a15f5fe_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/a15f5fe_politics2_1.jpg");
            // line 13
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("politics");
            echo "\">
            <img class=\"item\" src=\"";
            // line 14
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Politics\" />
        </a>
        ";
        } else {
            // asset "a15f5fe"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a15f5fe") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/a15f5fe.jpg");
            // line 13
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("politics");
            echo "\">
            <img class=\"item\" src=\"";
            // line 14
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Politics\" />
        </a>
        ";
        }
        unset($context["asset_url"]);
        // line 17
        echo "
        ";
        // line 18
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "1634b78_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_1634b78_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/1634b78_life2_1.jpg");
            // line 19
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("life");
            echo "\">
            <img class=\"item\" src=\"";
            // line 20
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Life\" />
        </a>
        ";
        } else {
            // asset "1634b78"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_1634b78") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/1634b78.jpg");
            // line 19
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("life");
            echo "\">
            <img class=\"item\" src=\"";
            // line 20
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Life\" />
        </a>
        ";
        }
        unset($context["asset_url"]);
        // line 23
        echo "        ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "01cde76_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_01cde76_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/01cde76_sport2_1.jpg");
            // line 24
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("sport");
            echo "\">
            <img class=\"item\" src=\"";
            // line 25
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Life\" />
        </a>
        ";
        } else {
            // asset "01cde76"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_01cde76") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/01cde76.jpg");
            // line 24
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("sport");
            echo "\">
            <img class=\"item\" src=\"";
            // line 25
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Life\" />
        </a>
        ";
        }
        unset($context["asset_url"]);
        // line 28
        echo "        ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb996c9_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bb996c9_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/bb996c9_tech2_1.jpg");
            // line 29
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("tech");
            echo "\">
            <img class=\"item\" src=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Tech\" />
        </a>
        ";
        } else {
            // asset "bb996c9"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bb996c9") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/bb996c9.jpg");
            // line 29
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("tech");
            echo "\">
            <img class=\"item\" src=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Tech\" />
        </a>
        ";
        }
        unset($context["asset_url"]);
        // line 33
        echo "        ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "075f250_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_075f250_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/075f250_all_1.jpg");
            // line 34
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("All");
            echo "\">
            <img class=\"item\" src=\"";
            // line 35
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"All\" />
        </a>
        ";
        } else {
            // asset "075f250"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_075f250") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/075f250.jpg");
            // line 34
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("All");
            echo "\">
            <img class=\"item\" src=\"";
            // line 35
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"All\" />
        </a>
        ";
        }
        unset($context["asset_url"]);
        // line 38
        echo "

    </div>


   ";
        
        $__internal_3adab021db27b5b98be7af41461a93137e058728cf000e10dbf84afd53a62349->leave($__internal_3adab021db27b5b98be7af41461a93137e058728cf000e10dbf84afd53a62349_prof);

    }

    public function getTemplateName()
    {
        return "CvutFitBiWT1BlogUiBundle:Post:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 38,  233 => 35,  228 => 34,  220 => 35,  215 => 34,  210 => 33,  203 => 30,  198 => 29,  190 => 30,  185 => 29,  180 => 28,  173 => 25,  168 => 24,  160 => 25,  155 => 24,  150 => 23,  143 => 20,  138 => 19,  130 => 20,  125 => 19,  121 => 18,  118 => 17,  111 => 14,  106 => 13,  98 => 14,  93 => 13,  89 => 12,  86 => 11,  79 => 8,  74 => 7,  66 => 8,  61 => 7,  57 => 6,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig' %}*/
/* {% block title %}Index{% endblock %}*/
/* {% block body %}*/
/*     <div class="categories">*/
/* */
/*             {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/trending2.jpg' %}*/
/*      <a href="{{ path('trending') }}">*/
/*         <img class="item" src="{{ asset_url }}" alt="Trending" />*/
/*     </a>*/
/*     {% endimage %}*/
/* */
/*         {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/politics2.jpg' %}*/
/*         <a href="{{ path('politics') }}">*/
/*             <img class="item" src="{{ asset_url }}" alt="Politics" />*/
/*         </a>*/
/*         {% endimage %}*/
/* */
/*         {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/life2.jpg' %}*/
/*         <a href="{{ path('life') }}">*/
/*             <img class="item" src="{{ asset_url }}" alt="Life" />*/
/*         </a>*/
/*         {% endimage %}*/
/*         {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/sport2.jpg' %}*/
/*         <a href="{{ path('sport') }}">*/
/*             <img class="item" src="{{ asset_url }}" alt="Life" />*/
/*         </a>*/
/*         {% endimage %}*/
/*         {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/tech2.jpg' %}*/
/*         <a href="{{ path('tech') }}">*/
/*             <img class="item" src="{{ asset_url }}" alt="Tech" />*/
/*         </a>*/
/*         {% endimage %}*/
/*         {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/all.jpg' %}*/
/*         <a href="{{ path('All') }}">*/
/*             <img class="item" src="{{ asset_url }}" alt="All" />*/
/*         </a>*/
/*         {% endimage %}*/
/* */
/* */
/*     </div>*/
/* */
/* */
/*    {% endblock %}*/
