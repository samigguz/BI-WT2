<?php

/* CvutFitBiWT1BlogUiBundle:Post:politics.html.twig */
class __TwigTemplate_8aa449026b7ab7c04e96108b77bb419e9cb10e84d5bb5d8d7693e06b41752d88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig", "CvutFitBiWT1BlogUiBundle:Post:politics.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f5c06662b82a7941c13a596d272ad8173b7e6a3a2106d0f54be91cf5f895bee = $this->env->getExtension("native_profiler");
        $__internal_8f5c06662b82a7941c13a596d272ad8173b7e6a3a2106d0f54be91cf5f895bee->enter($__internal_8f5c06662b82a7941c13a596d272ad8173b7e6a3a2106d0f54be91cf5f895bee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CvutFitBiWT1BlogUiBundle:Post:politics.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8f5c06662b82a7941c13a596d272ad8173b7e6a3a2106d0f54be91cf5f895bee->leave($__internal_8f5c06662b82a7941c13a596d272ad8173b7e6a3a2106d0f54be91cf5f895bee_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_1a4dd714d82bff206c96e94c8a7132deefbc4d72c4aaed899a3b181bb18e7e52 = $this->env->getExtension("native_profiler");
        $__internal_1a4dd714d82bff206c96e94c8a7132deefbc4d72c4aaed899a3b181bb18e7e52->enter($__internal_1a4dd714d82bff206c96e94c8a7132deefbc4d72c4aaed899a3b181bb18e7e52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, (isset($context["tag"]) ? $context["tag"] : $this->getContext($context, "tag")), "html", null, true);
        
        $__internal_1a4dd714d82bff206c96e94c8a7132deefbc4d72c4aaed899a3b181bb18e7e52->leave($__internal_1a4dd714d82bff206c96e94c8a7132deefbc4d72c4aaed899a3b181bb18e7e52_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b57e807541307b0505104aaf0f20cd133e14b0c03ce91bee55eb0d645cba8c4e = $this->env->getExtension("native_profiler");
        $__internal_b57e807541307b0505104aaf0f20cd133e14b0c03ce91bee55eb0d645cba8c4e->enter($__internal_b57e807541307b0505104aaf0f20cd133e14b0c03ce91bee55eb0d645cba8c4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "

    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts")));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 7
            echo "
        ";
            // line 8
            if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
                // line 9
                echo "            <p class=\"title\"><a class=\"title\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                echo "</a></p>
        ";
            } elseif ($this->getAttribute(            // line 10
$context["post"], "isPrivate", array())) {
                // line 11
                echo "            <p class=\"title\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                echo "</p>

        ";
            } else {
                // line 14
                echo "            <p class=\"title\"><a class=\"title\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                echo "</a></p>
        ";
            }
            // line 16
            echo "

        <div>
            ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "get150Text", array()), "html", null, true);
            echo "
            <br>
            ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["post"], "tags", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 22
                echo "                <span class=\"btn btn-success\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["tag"], "title", array()), "html", null, true);
                echo "</span>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "            ";
            if ($this->getAttribute($context["post"], "isPrivate", array())) {
                // line 25
                echo "                <span class=\"bg-danger\">Private post</span>
            ";
            }
            // line 27
            echo "
            <div class=\"date\">
                <time>";
            // line 29
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["post"], "created", array()), (isset($context["date_format"]) ? $context["date_format"] : $this->getContext($context, "date_format"))), "html", null, true);
            echo "</time>, ";
            if ($this->getAttribute($context["post"], "author", array())) {
                echo "<span class=\"author\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["post"], "author", array()), "name", array()), "html", null, true);
                echo "</span>";
            }
            // line 30
            echo "            </div>

        </div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
";
        
        $__internal_b57e807541307b0505104aaf0f20cd133e14b0c03ce91bee55eb0d645cba8c4e->leave($__internal_b57e807541307b0505104aaf0f20cd133e14b0c03ce91bee55eb0d645cba8c4e_prof);

    }

    public function getTemplateName()
    {
        return "CvutFitBiWT1BlogUiBundle:Post:politics.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 35,  132 => 30,  124 => 29,  120 => 27,  116 => 25,  113 => 24,  104 => 22,  100 => 21,  95 => 19,  90 => 16,  82 => 14,  75 => 11,  73 => 10,  66 => 9,  64 => 8,  61 => 7,  57 => 6,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig' %}*/
/* {% block title %}{{ tag }}{% endblock %}*/
/* {% block body %}*/
/* */
/* */
/*     {% for post in posts %}*/
/* */
/*         {% if is_granted('ROLE_USER') %}*/
/*             <p class="title"><a class="title" href="{{ path('detail', {'id':post.id}) }}" >{{ post.title }}</a></p>*/
/*         {% elseif post.isPrivate %}*/
/*             <p class="title">{{ post.title }}</p>*/
/* */
/*         {% else %}*/
/*             <p class="title"><a class="title" href="{{ path('detail', {'id':post.id}) }}" >{{ post.title }}</a></p>*/
/*         {% endif %}*/
/* */
/* */
/*         <div>*/
/*             {{ post.get150Text }}*/
/*             <br>*/
/*             {% for tag in post.tags %}*/
/*                 <span class="btn btn-success"> {{ tag.title }}</span>*/
/*             {% endfor %}*/
/*             {% if post.isPrivate %}*/
/*                 <span class="bg-danger">Private post</span>*/
/*             {% endif %}*/
/* */
/*             <div class="date">*/
/*                 <time>{{ post.created|date(date_format) }}</time>, {% if post.author %}<span class="author">{{ post.author.name }}</span>{% endif %}*/
/*             </div>*/
/* */
/*         </div>*/
/* */
/*     {% endfor %}*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
