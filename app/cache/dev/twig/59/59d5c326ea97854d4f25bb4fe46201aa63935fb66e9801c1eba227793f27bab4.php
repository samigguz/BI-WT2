<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_1c627af60a25cd7099428e7b595f5e8afd3a96ddce4f006e8efd8a25b8371bd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_82feb5e42ed7fdb782024ae0c42301a6d0fafc8d39fb568d2e821d9c41235d98 = $this->env->getExtension("native_profiler");
        $__internal_82feb5e42ed7fdb782024ae0c42301a6d0fafc8d39fb568d2e821d9c41235d98->enter($__internal_82feb5e42ed7fdb782024ae0c42301a6d0fafc8d39fb568d2e821d9c41235d98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_82feb5e42ed7fdb782024ae0c42301a6d0fafc8d39fb568d2e821d9c41235d98->leave($__internal_82feb5e42ed7fdb782024ae0c42301a6d0fafc8d39fb568d2e821d9c41235d98_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_eb45f056890ce53ae5becad5f1fd2b550a82b6de89387364ac82c8db045f3fd5 = $this->env->getExtension("native_profiler");
        $__internal_eb45f056890ce53ae5becad5f1fd2b550a82b6de89387364ac82c8db045f3fd5->enter($__internal_eb45f056890ce53ae5becad5f1fd2b550a82b6de89387364ac82c8db045f3fd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_eb45f056890ce53ae5becad5f1fd2b550a82b6de89387364ac82c8db045f3fd5->leave($__internal_eb45f056890ce53ae5becad5f1fd2b550a82b6de89387364ac82c8db045f3fd5_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_521951b54b7040e6983ce63987cce7529312ac454d5ea790c6af513eacdd01a3 = $this->env->getExtension("native_profiler");
        $__internal_521951b54b7040e6983ce63987cce7529312ac454d5ea790c6af513eacdd01a3->enter($__internal_521951b54b7040e6983ce63987cce7529312ac454d5ea790c6af513eacdd01a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_521951b54b7040e6983ce63987cce7529312ac454d5ea790c6af513eacdd01a3->leave($__internal_521951b54b7040e6983ce63987cce7529312ac454d5ea790c6af513eacdd01a3_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_cdc5abb7aedd37fa387675cf33dd85c46eb90a45c1b8fa5eda802bb75a941c47 = $this->env->getExtension("native_profiler");
        $__internal_cdc5abb7aedd37fa387675cf33dd85c46eb90a45c1b8fa5eda802bb75a941c47->enter($__internal_cdc5abb7aedd37fa387675cf33dd85c46eb90a45c1b8fa5eda802bb75a941c47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_cdc5abb7aedd37fa387675cf33dd85c46eb90a45c1b8fa5eda802bb75a941c47->leave($__internal_cdc5abb7aedd37fa387675cf33dd85c46eb90a45c1b8fa5eda802bb75a941c47_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
