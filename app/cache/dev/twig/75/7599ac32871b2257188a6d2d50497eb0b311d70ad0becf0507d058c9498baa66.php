<?php

/* CvutFitBiWT1BlogUiBundle::base.html.twig */
class __TwigTemplate_02a8cc32d76889a39b6b530950ffbcd567d6a838b8990182fe22c28111ac6f4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'title' => array($this, 'block_title'),
            'menu' => array($this, 'block_menu'),
            'body' => array($this, 'block_body'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9e7b71d067b6a111ff24de3ea41036646f09f6ddb605f694528e91945376b370 = $this->env->getExtension("native_profiler");
        $__internal_9e7b71d067b6a111ff24de3ea41036646f09f6ddb605f694528e91945376b370->enter($__internal_9e7b71d067b6a111ff24de3ea41036646f09f6ddb605f694528e91945376b370_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CvutFitBiWT1BlogUiBundle::base.html.twig"));

        // line 1
        echo "<!doctype html>
<html>
<head>
    <meta name=\"keywords\" content=\"\"/>
    <meta name=\"description\" content=\"\"/>
    <meta charset=\"UTF-8\"/>
    ";
        // line 7
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "0e28421_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0e28421_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/0e28421_normalize_1.css");
            // line 8
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "0e28421_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0e28421_1") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/0e28421_main_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "0e28421_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0e28421_2") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/0e28421_bootstrap.min_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "0e28421"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0e28421") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/0e28421.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 10
        echo "
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>
    <script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.min.js\"></script>

    ";
        // line 14
        $this->displayBlock('javascripts', $context, $blocks);
        // line 27
        echo "
 ";
        // line 33
        echo "    <title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
</head>
<body>
<header>


    ";
        // line 39
        $this->displayBlock('menu', $context, $blocks);
        // line 77
        echo "</header>
";
        // line 79
        echo "    ";
        // line 81
        echo "<hr class=\"cleaner\" />
<div class=\"container\" id=\"content\">

    <article class=\"content-left\">
        ";
        // line 85
        $this->displayBlock('body', $context, $blocks);
        // line 88
        echo "    </article>

    ";
        // line 91
        echo "    ";
        // line 92
        echo "    <aside class=\"content-right\">
        ";
        // line 93
        $this->displayBlock('sidebar', $context, $blocks);
        // line 110
        echo "    </aside>


</div>
<footer class=\"container\">
    ";
        // line 115
        $this->displayBlock('footer', $context, $blocks);
        // line 118
        echo "</footer>

</body>
</html>";
        
        $__internal_9e7b71d067b6a111ff24de3ea41036646f09f6ddb605f694528e91945376b370->leave($__internal_9e7b71d067b6a111ff24de3ea41036646f09f6ddb605f694528e91945376b370_prof);

    }

    // line 14
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f6fd398c9d6f6f90813def724efe0592b59fb2c47ab54266d3d8a31573e89373 = $this->env->getExtension("native_profiler");
        $__internal_f6fd398c9d6f6f90813def724efe0592b59fb2c47ab54266d3d8a31573e89373->enter($__internal_f6fd398c9d6f6f90813def724efe0592b59fb2c47ab54266d3d8a31573e89373_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 15
        echo "     ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "0a79fba_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0a79fba_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0a79fba_skript2_1.js");
            // line 16
            echo "      <script>

     var script = document.createElement('script');
     script.onload = function() {
        // alert(\"Script loaded and ready\");
     };
     script.src =\"";
            // line 22
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\";
     document.getElementsByTagName('head')[0].appendChild(script);</script>

    ";
        } else {
            // asset "0a79fba"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0a79fba") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0a79fba.js");
            // line 16
            echo "      <script>

     var script = document.createElement('script');
     script.onload = function() {
        // alert(\"Script loaded and ready\");
     };
     script.src =\"";
            // line 22
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\";
     document.getElementsByTagName('head')[0].appendChild(script);</script>

    ";
        }
        unset($context["asset_url"]);
        // line 26
        echo "    ";
        
        $__internal_f6fd398c9d6f6f90813def724efe0592b59fb2c47ab54266d3d8a31573e89373->leave($__internal_f6fd398c9d6f6f90813def724efe0592b59fb2c47ab54266d3d8a31573e89373_prof);

    }

    // line 33
    public function block_title($context, array $blocks = array())
    {
        $__internal_18bbc3696aad0971add2c1d137caa6008c84cde5a01fc9255b1a118b8695f538 = $this->env->getExtension("native_profiler");
        $__internal_18bbc3696aad0971add2c1d137caa6008c84cde5a01fc9255b1a118b8695f538->enter($__internal_18bbc3696aad0971add2c1d137caa6008c84cde5a01fc9255b1a118b8695f538_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "BI-WT1 | Blog";
        
        $__internal_18bbc3696aad0971add2c1d137caa6008c84cde5a01fc9255b1a118b8695f538->leave($__internal_18bbc3696aad0971add2c1d137caa6008c84cde5a01fc9255b1a118b8695f538_prof);

    }

    // line 39
    public function block_menu($context, array $blocks = array())
    {
        $__internal_22bdc246a56440ab3b35c7efc74a3d83ee32a37c308dafbac56d47945bd4c35b = $this->env->getExtension("native_profiler");
        $__internal_22bdc246a56440ab3b35c7efc74a3d83ee32a37c308dafbac56d47945bd4c35b->enter($__internal_22bdc246a56440ab3b35c7efc74a3d83ee32a37c308dafbac56d47945bd4c35b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 40
        echo "    <nav class=\"menu\">


            <div class=\"logo-box\">
                <h1 class=\"logo\">
                    ";
        // line 45
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "faf7dd2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_faf7dd2_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/faf7dd2_ll_1.png");
            // line 46
            echo "                    <a href=\"";
            echo $this->env->getExtension('routing')->getPath("home");
            echo "\">
                        <img class=\"logo\" src=\"";
            // line 47
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Blog logo\" height=\"100\" width=\"150\"/>
                    </a>
                    ";
        } else {
            // asset "faf7dd2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_faf7dd2") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/faf7dd2.png");
            // line 46
            echo "                    <a href=\"";
            echo $this->env->getExtension('routing')->getPath("home");
            echo "\">
                        <img class=\"logo\" src=\"";
            // line 47
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Blog logo\" height=\"100\" width=\"150\"/>
                    </a>
                    ";
        }
        unset($context["asset_url"]);
        // line 50
        echo "                </h1>
            </div>
        ";
        // line 52
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 53
            echo "            <p class=\"username\" >Username: ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
            echo "</p>
        ";
        }
        // line 55
        echo "        <ul>
            <li><a href=\"/\">home</a></li>
            <li><a href=\"/trending\">Trending</a></li>
            <li><a href=\"/politics\">Politics</a></li>
            <li><a href=\"/life\">Life</a></li>
            <li><a href=\"/sport\">Sport</a></li>
            <li><a href=\"/All\">All</a></li>

            <li class=\"log1\"><a href=\"/login\">Login
                    ";
        // line 64
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "0a9f304_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0a9f304_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/0a9f304_login_1.png");
            // line 65
            echo "                    <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Blog login\" height=\"32\" width=\"37\"/>
                    ";
        } else {
            // asset "0a9f304"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0a9f304") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/0a9f304.png");
            echo "                    <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"Blog login\" height=\"32\" width=\"37\"/>
                    ";
        }
        unset($context["asset_url"]);
        // line 66
        echo "</a>

                ";
        // line 68
        if (($this->env->getExtension('security')->isGranted("ROLE_USER") || $this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) {
            // line 69
            echo "            <li class=\"log2\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\">logout
                    ";
            // line 70
            if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
                // asset "a1ed59a_0"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a1ed59a_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/a1ed59a_logout_1.png");
                // line 71
                echo "                    <img src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
                echo "\" alt=\"Blog login\" height=\"32\" width=\"37\"/>
                    ";
            } else {
                // asset "a1ed59a"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a1ed59a") : $this->env->getExtension('asset')->getAssetUrl("_controller/images/a1ed59a.png");
                echo "                    <img src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
                echo "\" alt=\"Blog login\" height=\"32\" width=\"37\"/>
                    ";
            }
            unset($context["asset_url"]);
            // line 72
            echo "</a></li>
        </ul>
        ";
        }
        // line 75
        echo "    </nav>
    ";
        
        $__internal_22bdc246a56440ab3b35c7efc74a3d83ee32a37c308dafbac56d47945bd4c35b->leave($__internal_22bdc246a56440ab3b35c7efc74a3d83ee32a37c308dafbac56d47945bd4c35b_prof);

    }

    // line 85
    public function block_body($context, array $blocks = array())
    {
        $__internal_51fd33af3b434e02beff1e9b51ca5334a6173febbd4a91635e74317498bb8d32 = $this->env->getExtension("native_profiler");
        $__internal_51fd33af3b434e02beff1e9b51ca5334a6173febbd4a91635e74317498bb8d32->enter($__internal_51fd33af3b434e02beff1e9b51ca5334a6173febbd4a91635e74317498bb8d32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 86
        echo "
        ";
        
        $__internal_51fd33af3b434e02beff1e9b51ca5334a6173febbd4a91635e74317498bb8d32->leave($__internal_51fd33af3b434e02beff1e9b51ca5334a6173febbd4a91635e74317498bb8d32_prof);

    }

    // line 93
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_a17fe90cd0a8cd028a8e9021a948bf679ec4444d93597b20ae903922379c5259 = $this->env->getExtension("native_profiler");
        $__internal_a17fe90cd0a8cd028a8e9021a948bf679ec4444d93597b20ae903922379c5259->enter($__internal_a17fe90cd0a8cd028a8e9021a948bf679ec4444d93597b20ae903922379c5259_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 94
        echo "
            <nav class=\"kategorie\">
                <h2>Filtering</h2>
              <ol>
                  <li><a href=\"/author\">By Author</a></li>
                  <li><a href=\"/date\">By Date</a> </li>
                  <li><a href=\"/private\">By Private</a> </li>
                  <li><a href=\"/tag\">By Tags</a> </li>
               </ol>
            </nav>
        ";
        // line 104
        if (($this->env->getExtension('security')->isGranted("ROLE_USER") || $this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) {
            // line 105
            echo "            <a href=\"/new\" class=\"btn-primary\">Add post</a>

";
        }
        // line 108
        echo "
        ";
        
        $__internal_a17fe90cd0a8cd028a8e9021a948bf679ec4444d93597b20ae903922379c5259->leave($__internal_a17fe90cd0a8cd028a8e9021a948bf679ec4444d93597b20ae903922379c5259_prof);

    }

    // line 115
    public function block_footer($context, array $blocks = array())
    {
        $__internal_15b2a61d5d4d05f9414a8f5c34ce0a605c6709dc69a6449d3db29bcc1f7932c9 = $this->env->getExtension("native_profiler");
        $__internal_15b2a61d5d4d05f9414a8f5c34ce0a605c6709dc69a6449d3db29bcc1f7932c9->enter($__internal_15b2a61d5d4d05f9414a8f5c34ce0a605c6709dc69a6449d3db29bcc1f7932c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 116
        echo "    &copy; Samigullina Guzel 2015
    ";
        
        $__internal_15b2a61d5d4d05f9414a8f5c34ce0a605c6709dc69a6449d3db29bcc1f7932c9->leave($__internal_15b2a61d5d4d05f9414a8f5c34ce0a605c6709dc69a6449d3db29bcc1f7932c9_prof);

    }

    public function getTemplateName()
    {
        return "CvutFitBiWT1BlogUiBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  373 => 116,  367 => 115,  359 => 108,  354 => 105,  352 => 104,  340 => 94,  334 => 93,  326 => 86,  320 => 85,  312 => 75,  307 => 72,  293 => 71,  289 => 70,  284 => 69,  282 => 68,  278 => 66,  264 => 65,  260 => 64,  249 => 55,  243 => 53,  241 => 52,  237 => 50,  230 => 47,  225 => 46,  217 => 47,  212 => 46,  208 => 45,  201 => 40,  195 => 39,  183 => 33,  176 => 26,  168 => 22,  160 => 16,  151 => 22,  143 => 16,  138 => 15,  132 => 14,  122 => 118,  120 => 115,  113 => 110,  111 => 93,  108 => 92,  106 => 91,  102 => 88,  100 => 85,  94 => 81,  92 => 79,  89 => 77,  87 => 39,  77 => 33,  74 => 27,  72 => 14,  66 => 10,  40 => 8,  36 => 7,  28 => 1,);
    }
}
/* <!doctype html>*/
/* <html>*/
/* <head>*/
/*     <meta name="keywords" content=""/>*/
/*     <meta name="description" content=""/>*/
/*     <meta charset="UTF-8"/>*/
/*     {% stylesheets '@CvutFitBiWT1BlogUiBundle/Resources/public/css/normalize.css' '@CvutFitBiWT1BlogUiBundle/Resources/public/css/main.css' '@CvutFitBiWT1BlogUiBundle/Resources/public/css/css/bootstrap.min.css' %}*/
/*     <link rel="stylesheet" href="{{ asset_url }}" />*/
/*     {% endstylesheets %}*/
/* */
/*     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>*/
/*     <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.min.js"></script>*/
/* */
/*     {% block javascripts %}*/
/*      {% javascripts '@CvutFitBiWT1BlogUiBundle/Resources/public/js/skript2.js' %}*/
/*       <script>*/
/* */
/*      var script = document.createElement('script');*/
/*      script.onload = function() {*/
/*         // alert("Script loaded and ready");*/
/*      };*/
/*      script.src ="{{ asset_url }}";*/
/*      document.getElementsByTagName('head')[0].appendChild(script);</script>*/
/* */
/*     {% endjavascripts %}*/
/*     {% endblock %}*/
/* */
/*  {#% block javascripts %}*/
/*      {% javascripts '@CvutFitBiWT1BlogUiBundle/Resources/public/js/skript.js' %}*/
/*      <script type="text/javascript" src="{{ asset_url }}"></script>*/
/*      {% endjavascripts %}*/
/*  {% endblock %#}*/
/*     <title>{% block title %}BI-WT1 | Blog{% endblock %}</title>*/
/* </head>*/
/* <body>*/
/* <header>*/
/* */
/* */
/*     {% block menu %}*/
/*     <nav class="menu">*/
/* */
/* */
/*             <div class="logo-box">*/
/*                 <h1 class="logo">*/
/*                     {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/ll.png' %}*/
/*                     <a href="{{ path('home') }}">*/
/*                         <img class="logo" src="{{ asset_url }}" alt="Blog logo" height="100" width="150"/>*/
/*                     </a>*/
/*                     {% endimage %}*/
/*                 </h1>*/
/*             </div>*/
/*         {% if is_granted('IS_AUTHENTICATED_FULLY') %}*/
/*             <p class="username" >Username: {{ app.user.username}}</p>*/
/*         {% endif %}*/
/*         <ul>*/
/*             <li><a href="/">home</a></li>*/
/*             <li><a href="/trending">Trending</a></li>*/
/*             <li><a href="/politics">Politics</a></li>*/
/*             <li><a href="/life">Life</a></li>*/
/*             <li><a href="/sport">Sport</a></li>*/
/*             <li><a href="/All">All</a></li>*/
/* */
/*             <li class="log1"><a href="/login">Login*/
/*                     {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/login.png' %}*/
/*                     <img src="{{ asset_url }}" alt="Blog login" height="32" width="37"/>*/
/*                     {% endimage %}</a>*/
/* */
/*                 {% if is_granted('ROLE_USER') or is_granted('ROLE_ADMIN')%}*/
/*             <li class="log2"><a href="{{ path('logout') }}">logout*/
/*                     {% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/logout.png' %}*/
/*                     <img src="{{ asset_url }}" alt="Blog login" height="32" width="37"/>*/
/*                     {% endimage %}</a></li>*/
/*         </ul>*/
/*         {% endif %}*/
/*     </nav>*/
/*     {%  endblock %}*/
/* </header>*/
/* {#{% if not is_granted('ROLE_USER') and not is_granted('ROLE_ADMIN') %}#}*/
/*     {#<h2>Prihlaseni uzivatele:</h2>#}*/
/* {#{% endif %}#}*/
/* <hr class="cleaner" />*/
/* <div class="container" id="content">*/
/* */
/*     <article class="content-left">*/
/*         {% block body %}*/
/* */
/*         {% endblock %}*/
/*     </article>*/
/* */
/*     {#{% if is_granted('ROLE_USER') or is_granted('ROLE_ADMIN')%}#}*/
/*     {#{% endif %}#}*/
/*     <aside class="content-right">*/
/*         {% block sidebar %}*/
/* */
/*             <nav class="kategorie">*/
/*                 <h2>Filtering</h2>*/
/*               <ol>*/
/*                   <li><a href="/author">By Author</a></li>*/
/*                   <li><a href="/date">By Date</a> </li>*/
/*                   <li><a href="/private">By Private</a> </li>*/
/*                   <li><a href="/tag">By Tags</a> </li>*/
/*                </ol>*/
/*             </nav>*/
/*         {% if is_granted('ROLE_USER') or is_granted('ROLE_ADMIN')%}*/
/*             <a href="/new" class="btn-primary">Add post</a>*/
/* */
/* {% endif %}*/
/* */
/*         {% endblock %}*/
/*     </aside>*/
/* */
/* */
/* </div>*/
/* <footer class="container">*/
/*     {% block footer %}*/
/*     &copy; Samigullina Guzel 2015*/
/*     {% endblock %}*/
/* </footer>*/
/* */
/* </body>*/
/* </html>*/
