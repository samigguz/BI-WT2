<?php

/* CvutFitBiWT1BlogUiBundle:Post:detail.html.twig */
class __TwigTemplate_cc176d75e9d3bc3e5dd6ad27ace8f91b9ce03a3519cbdee34fcfc075b905363d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig", "CvutFitBiWT1BlogUiBundle:Post:detail.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_51339c6a9d07d5db32601508aed8908ec96a4f1be9176e08ca0e2639194f7bb4 = $this->env->getExtension("native_profiler");
        $__internal_51339c6a9d07d5db32601508aed8908ec96a4f1be9176e08ca0e2639194f7bb4->enter($__internal_51339c6a9d07d5db32601508aed8908ec96a4f1be9176e08ca0e2639194f7bb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CvutFitBiWT1BlogUiBundle:Post:detail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_51339c6a9d07d5db32601508aed8908ec96a4f1be9176e08ca0e2639194f7bb4->leave($__internal_51339c6a9d07d5db32601508aed8908ec96a4f1be9176e08ca0e2639194f7bb4_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_96f991f720382ec616687f050295554543ea252e3597733e4b45c645e059ae5e = $this->env->getExtension("native_profiler");
        $__internal_96f991f720382ec616687f050295554543ea252e3597733e4b45c645e059ae5e->enter($__internal_96f991f720382ec616687f050295554543ea252e3597733e4b45c645e059ae5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Post";
        
        $__internal_96f991f720382ec616687f050295554543ea252e3597733e4b45c645e059ae5e->leave($__internal_96f991f720382ec616687f050295554543ea252e3597733e4b45c645e059ae5e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c52dbcd895183b94932bea88d9c83df3ddb60c54759114987e588eed22e1253b = $this->env->getExtension("native_profiler");
        $__internal_c52dbcd895183b94932bea88d9c83df3ddb60c54759114987e588eed22e1253b->enter($__internal_c52dbcd895183b94932bea88d9c83df3ddb60c54759114987e588eed22e1253b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <p><a href=\"/\">Blog</a> >>><a href='/'+";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "getTag", array()), "html", null, true);
        echo "> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "getTag", array()), "html", null, true);
        echo "</a>  >>> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "getSmallTitle", array()), "html", null, true);
        echo "</p>
    <p class=\"title\"> ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "title", array()), "html", null, true);
        echo "</p>
    <p>Publish from <time> ";
        // line 6
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "publishFrom", array()), (isset($context["date_format2"]) ? $context["date_format2"] : $this->getContext($context, "date_format2"))), "html", null, true);
        echo "</time> to <time>";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "publishTo", array()), (isset($context["date_format2"]) ? $context["date_format2"] : $this->getContext($context, "date_format2"))), "html", null, true);
        echo "</time></p>

    ";
        // line 8
        if ($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "file", array())) {
            // line 9
            echo "    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("images/" . $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "file", array()))), "html", null, true);
            echo "\" height=\"300\" width=\"500\"/>
        ";
        }
        // line 11
        echo "
    <p class=\"text\"> ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "getText", array()), "html", null, true);
        echo "</p>


    <section id=\"comments\">

        <div class=\"date\">
            <time>";
        // line 18
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "getCreated", array()), (isset($context["date_format"]) ? $context["date_format"] : $this->getContext($context, "date_format"))), "html", null, true);
        echo "</time>,";
        if ($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "author", array())) {
            echo "<span class=\"author\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "author", array()), "name", array()), "html", null, true);
            echo "</span>";
        }
        // line 19
        echo "        </div>


        ";
        // line 23
        echo "
        ";
        // line 25
        echo "        ";
        // line 26
        echo "
    ";
        // line 27
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            // line 28
            echo "
        ";
            // line 29
            if ($this->env->getExtension('security')->isGranted("edit", (isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")))) {
                // line 30
                echo "        <a href=\"/post/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "getId", array()), "html", null, true);
                echo "/delete\" class=\"btn-primary\">Delete post</a>
        <a href=\"/update/";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "getId", array()), "html", null, true);
                echo "\" class=\"btn-primary\">Update post</a>
       ";
            }
            // line 33
            echo "
        <h2>Comments</h2>

        ";
            // line 36
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
            echo "
        ";
            // line 37
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
            echo "
        ";
            // line 38
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
            echo "

        ";
            // line 40
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["comments"]) ? $context["comments"] : $this->getContext($context, "comments")), "getIterator", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
                // line 41
                echo "            ";
                $this->loadTemplate("@CvutFitBiWT1BlogUiBundle/Resources/views/Post/comments.html.twig", "CvutFitBiWT1BlogUiBundle:Post:detail.html.twig", 41)->display(array_merge($context, array("comment" => $context["comment"], "post" => (isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")))));
                // line 42
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "


        <nav id=\"divPaging\">
            <ul class=\"paging\">

                ";
            // line 49
            if (((isset($context["maxPages"]) ? $context["maxPages"] : $this->getContext($context, "maxPages")) > 1)) {
                // line 50
                echo "                    ";
                if (((isset($context["thisPage"]) ? $context["thisPage"] : $this->getContext($context, "thisPage")) > 1)) {
                    // line 51
                    echo "                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail_page", array("page" => ((isset($context["thisPage"]) ? $context["thisPage"] : $this->getContext($context, "thisPage")) - 1), "id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "id", array()))), "html", null, true);
                    echo "\"><< </a>
                    ";
                }
                // line 53
                echo "                    ";
                echo twig_escape_filter($this->env, (isset($context["thisPage"]) ? $context["thisPage"] : $this->getContext($context, "thisPage")), "html", null, true);
                echo "
                    ";
                // line 54
                if (((isset($context["thisPage"]) ? $context["thisPage"] : $this->getContext($context, "thisPage")) < (isset($context["maxPages"]) ? $context["maxPages"] : $this->getContext($context, "maxPages")))) {
                    // line 55
                    echo "                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail_page", array("page" => ((isset($context["thisPage"]) ? $context["thisPage"] : $this->getContext($context, "thisPage")) + 1), "id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "id", array()))), "html", null, true);
                    echo "\"> >></a>
                    ";
                }
                // line 57
                echo "                ";
            }
            // line 58
            echo "
                ";
            // line 59
            if (((isset($context["replyy"]) ? $context["replyy"] : $this->getContext($context, "replyy")) == "1")) {
                // line 60
                echo "
                ";
            }
            // line 62
            echo "                ";
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
            echo "
            </ul>
        </nav>
    ";
        }
        // line 66
        echo "    </section>

";
        
        $__internal_c52dbcd895183b94932bea88d9c83df3ddb60c54759114987e588eed22e1253b->leave($__internal_c52dbcd895183b94932bea88d9c83df3ddb60c54759114987e588eed22e1253b_prof);

    }

    public function getTemplateName()
    {
        return "CvutFitBiWT1BlogUiBundle:Post:detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 66,  227 => 62,  223 => 60,  221 => 59,  218 => 58,  215 => 57,  209 => 55,  207 => 54,  202 => 53,  196 => 51,  193 => 50,  191 => 49,  183 => 43,  169 => 42,  166 => 41,  149 => 40,  144 => 38,  140 => 37,  136 => 36,  131 => 33,  126 => 31,  121 => 30,  119 => 29,  116 => 28,  114 => 27,  111 => 26,  109 => 25,  106 => 23,  101 => 19,  93 => 18,  84 => 12,  81 => 11,  75 => 9,  73 => 8,  66 => 6,  62 => 5,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig' %}*/
/* {% block title %}Post{% endblock %}*/
/* {% block body %}*/
/*     <p><a href="/">Blog</a> >>><a href='/'+{{ post.getTag }}> {{post.getTag}}</a>  >>> {{ post.getSmallTitle }}</p>*/
/*     <p class="title"> {{ post.title }}</p>*/
/*     <p>Publish from <time> {{ post.publishFrom|date(date_format2) }}</time> to <time>{{ post.publishTo|date(date_format2) }}</time></p>*/
/* */
/*     {% if post.file %}*/
/*     <img src="{{ asset('images/' ~ post.file ) }}" height="300" width="500"/>*/
/*         {% endif %}*/
/* */
/*     <p class="text"> {{ post.getText }}</p>*/
/* */
/* */
/*     <section id="comments">*/
/* */
/*         <div class="date">*/
/*             <time>{{ post.getCreated|date(date_format) }}</time>,{% if post.author %}<span class="author">{{ post.author.name }}</span>{% endif %}*/
/*         </div>*/
/* */
/* */
/*         {#{% image '@CvutFitBiWT1BlogUiBundle/Resources/public/img/' %}#}*/
/* */
/*         {#<img src="{{ asset_url ~ post.file}}" alt="Blog login" height="32" width="37"/>#}*/
/*         {#{% endimage %}#}*/
/* */
/*     {% if is_granted('ROLE_USER') %}*/
/* */
/*         {% if is_granted('edit',post) %}*/
/*         <a href="/post/{{ post.getId }}/delete" class="btn-primary">Delete post</a>*/
/*         <a href="/update/{{ post.getId }}" class="btn-primary">Update post</a>*/
/*        {% endif %}*/
/* */
/*         <h2>Comments</h2>*/
/* */
/*         {{ form_start(form) }}*/
/*         {{ form_widget(form) }}*/
/*         {{ form_end(form) }}*/
/* */
/*         {% for comment in comments.getIterator %}*/
/*             {% include "@CvutFitBiWT1BlogUiBundle/Resources/views/Post/comments.html.twig" with {'comment': comment,'post':post} %}*/
/*         {% endfor %}*/
/* */
/* */
/* */
/*         <nav id="divPaging">*/
/*             <ul class="paging">*/
/* */
/*                 {% if maxPages > 1 %}*/
/*                     {% if thisPage > 1 %}*/
/*                         <a href="{{ path('detail_page', {'page' : thisPage-1, 'id' : post.id}) }}"><< </a>*/
/*                     {% endif %}*/
/*                     {{ thisPage }}*/
/*                     {% if thisPage < maxPages %}*/
/*                         <a href="{{ path('detail_page', {'page' : thisPage + 1, 'id' : post.id}) }}"> >></a>*/
/*                     {% endif %}*/
/*                 {% endif %}*/
/* */
/*                 {% if replyy == "1" %}*/
/* */
/*                 {% endif %}*/
/*                 {{ form(form) }}*/
/*             </ul>*/
/*         </nav>*/
/*     {% endif %}*/
/*     </section>*/
/* */
/* {% endblock %}*/
