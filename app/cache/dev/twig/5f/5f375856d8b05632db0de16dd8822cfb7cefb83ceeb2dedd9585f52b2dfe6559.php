<?php

/* CvutFitBiWT1BlogUiBundle:Post:all.html.twig */
class __TwigTemplate_5892a54ecb8078c69e0518c9bc9c222d77e6e22ba9e775e8c792b0f8ff3f8be4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig", "CvutFitBiWT1BlogUiBundle:Post:all.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b87d3660b6c6f71912cf9f6165ae084181c9ba80ca128a79f3bdead91494a9df = $this->env->getExtension("native_profiler");
        $__internal_b87d3660b6c6f71912cf9f6165ae084181c9ba80ca128a79f3bdead91494a9df->enter($__internal_b87d3660b6c6f71912cf9f6165ae084181c9ba80ca128a79f3bdead91494a9df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CvutFitBiWT1BlogUiBundle:Post:all.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b87d3660b6c6f71912cf9f6165ae084181c9ba80ca128a79f3bdead91494a9df->leave($__internal_b87d3660b6c6f71912cf9f6165ae084181c9ba80ca128a79f3bdead91494a9df_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_994b0b8b085d03770a0f8f752f02afdf69a17159a4d4bf299fbeb3dcfe54af19 = $this->env->getExtension("native_profiler");
        $__internal_994b0b8b085d03770a0f8f752f02afdf69a17159a4d4bf299fbeb3dcfe54af19->enter($__internal_994b0b8b085d03770a0f8f752f02afdf69a17159a4d4bf299fbeb3dcfe54af19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Index";
        
        $__internal_994b0b8b085d03770a0f8f752f02afdf69a17159a4d4bf299fbeb3dcfe54af19->leave($__internal_994b0b8b085d03770a0f8f752f02afdf69a17159a4d4bf299fbeb3dcfe54af19_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_663fb51802460f450d8724d937ad3c823e78d507aac699880a3e646fdb1ecb6a = $this->env->getExtension("native_profiler");
        $__internal_663fb51802460f450d8724d937ad3c823e78d507aac699880a3e646fdb1ecb6a->enter($__internal_663fb51802460f450d8724d937ad3c823e78d507aac699880a3e646fdb1ecb6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    ";
        // line 5
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "id"), "method") > 0)) {
            // line 6
            echo "        ";
            $context["startPage"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "id"), "method");
            // line 7
            echo "        ";
        } else {
            // line 8
            echo "        ";
            $context["startPage"] = 1;
            // line 9
            echo "    ";
        }
        // line 10
        echo "
    ";
        // line 11
        $context["i"] = 1;
        // line 12
        echo "<div class=\"tmp\">
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts")));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 14
            echo "        ";
            if (((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) < (4 + (isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage"))))) {
                // line 15
                echo "            ";
                if (((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) >= (isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")))) {
                    // line 16
                    echo "
                ";
                    // line 17
                    if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
                        // line 18
                        echo "                    <p class=\"title\"><a class=\"title\" href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
                        echo "\" >";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                        echo "</a></p>
                ";
                    } elseif ($this->getAttribute(                    // line 19
$context["post"], "isPrivate", array())) {
                        // line 20
                        echo "                    <p class=\"title\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                        echo "</p>

                    ";
                    } else {
                        // line 23
                        echo "                    <p class=\"title\"><a class=\"title\" href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
                        echo "\" >";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
                        echo "</a></p>
                ";
                    }
                    // line 25
                    echo "        <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "get150Text", array()), "html", null, true);
                    echo "</p>
    ";
                    // line 26
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["post"], "tags", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                        // line 27
                        echo "       <span class=\"btn btn-success\"> ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["tag"], "title", array()), "html", null, true);
                        echo "</span>
        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 29
                    echo "                ";
                    if ($this->getAttribute($context["post"], "isPrivate", array())) {
                        // line 30
                        echo "                    <span class=\"bg-danger\">Private post</span>
                ";
                    }
                    // line 32
                    echo "        <div class=\"date\">
            <time>";
                    // line 33
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["post"], "created", array()), (isset($context["date_format"]) ? $context["date_format"] : $this->getContext($context, "date_format"))), "html", null, true);
                    echo "</time>, ";
                    if ($this->getAttribute($context["post"], "author", array())) {
                        echo "<span class=\"author\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["post"], "author", array()), "name", array()), "html", null, true);
                        echo "</span>";
                    }
                    // line 34
                    echo "        </div>


            ";
                }
                // line 38
                echo "        ";
            }
            // line 39
            echo "        ";
            $context["i"] = ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1);
            // line 40
            echo "

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "
</div>


    ";
        // line 47
        $this->displayBlock('javascripts', $context, $blocks);
        // line 52
        echo "
    <nav id=\"divPaging\">
        <ul class=\"paging\">
            ";
        // line 55
        if (((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) >= 4)) {
            // line 56
            echo "            <li><a class=\"btn-info\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("All", array("id" => ((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) - 4))), "html", null, true);
            echo "\"> Page ";
            echo twig_escape_filter($this->env, ((((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) / 4) + 0.75) - 1), "html", null, true);
            echo " </a></li>
             ";
        }
        // line 58
        echo "            <span>Page ";
        echo twig_escape_filter($this->env, (((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) / 4) + 0.75), "html", null, true);
        echo " </span>
            ";
        // line 59
        if ((((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) + 4) <= (isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")))) {
            // line 60
            echo "            <li><a class=\"btn-info\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("All", array("id" => ((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) + 4))), "html", null, true);
            echo "\"> Page ";
            echo twig_escape_filter($this->env, (((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) / 4) + 1.75), "html", null, true);
            echo " </a></li>
            ";
        }
        // line 62
        echo "        </ul>
    </nav>
";
        
        $__internal_663fb51802460f450d8724d937ad3c823e78d507aac699880a3e646fdb1ecb6a->leave($__internal_663fb51802460f450d8724d937ad3c823e78d507aac699880a3e646fdb1ecb6a_prof);

    }

    // line 47
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_9e78c6a74a86cfce07b776c19d0a934b1af1eb9be6633635b0f014c217d8b4bb = $this->env->getExtension("native_profiler");
        $__internal_9e78c6a74a86cfce07b776c19d0a934b1af1eb9be6633635b0f014c217d8b4bb->enter($__internal_9e78c6a74a86cfce07b776c19d0a934b1af1eb9be6633635b0f014c217d8b4bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 48
        echo "        ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "fd72976_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_fd72976_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/fd72976_skript_1.js");
            // line 49
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
        ";
        } else {
            // asset "fd72976"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_fd72976") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/fd72976.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
        ";
        }
        unset($context["asset_url"]);
        // line 51
        echo "    ";
        
        $__internal_9e78c6a74a86cfce07b776c19d0a934b1af1eb9be6633635b0f014c217d8b4bb->leave($__internal_9e78c6a74a86cfce07b776c19d0a934b1af1eb9be6633635b0f014c217d8b4bb_prof);

    }

    public function getTemplateName()
    {
        return "CvutFitBiWT1BlogUiBundle:Post:all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  246 => 51,  232 => 49,  227 => 48,  221 => 47,  212 => 62,  204 => 60,  202 => 59,  197 => 58,  189 => 56,  187 => 55,  182 => 52,  180 => 47,  174 => 43,  166 => 40,  163 => 39,  160 => 38,  154 => 34,  146 => 33,  143 => 32,  139 => 30,  136 => 29,  127 => 27,  123 => 26,  118 => 25,  110 => 23,  103 => 20,  101 => 19,  94 => 18,  92 => 17,  89 => 16,  86 => 15,  83 => 14,  79 => 13,  76 => 12,  74 => 11,  71 => 10,  68 => 9,  65 => 8,  62 => 7,  59 => 6,  57 => 5,  54 => 4,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '@CvutFitBiWT1BlogUiBundle/Resources/views/base.html.twig' %}*/
/* {% block title %}Index{% endblock %}*/
/* {% block body %}*/
/* */
/*     {% if  app.request.get('id')>0 %}*/
/*         {% set startPage=app.request.get('id')%}*/
/*         {% else  %}*/
/*         {% set startPage=1 %}*/
/*     {% endif %}*/
/* */
/*     {% set i=1 %}*/
/* <div class="tmp">*/
/*     {% for post in posts %}*/
/*         {% if i<4+startPage %}*/
/*             {% if i>=startPage %}*/
/* */
/*                 {% if is_granted('ROLE_USER') %}*/
/*                     <p class="title"><a class="title" href="{{ path('detail', {'id':post.id}) }}" >{{ post.title }}</a></p>*/
/*                 {% elseif post.isPrivate %}*/
/*                     <p class="title">{{ post.title }}</p>*/
/* */
/*                     {% else %}*/
/*                     <p class="title"><a class="title" href="{{ path('detail', {'id':post.id}) }}" >{{ post.title }}</a></p>*/
/*                 {% endif %}*/
/*         <p>{{ post.get150Text }}</p>*/
/*     {% for tag in post.tags %}*/
/*        <span class="btn btn-success"> {{ tag.title }}</span>*/
/*         {% endfor %}*/
/*                 {% if post.isPrivate %}*/
/*                     <span class="bg-danger">Private post</span>*/
/*                 {% endif %}*/
/*         <div class="date">*/
/*             <time>{{ post.created|date(date_format) }}</time>, {% if post.author %}<span class="author">{{ post.author.name }}</span>{% endif %}*/
/*         </div>*/
/* */
/* */
/*             {% endif %}*/
/*         {% endif %}*/
/*         {% set i=i+1 %}*/
/* */
/* */
/*     {% endfor %}*/
/* */
/* </div>*/
/* */
/* */
/*     {% block javascripts %}*/
/*         {% javascripts '@CvutFitBiWT1BlogUiBundle/Resources/public/js/skript.js' %}*/
/*         <script type="text/javascript" src="{{ asset_url }}"></script>*/
/*         {% endjavascripts %}*/
/*     {% endblock %}*/
/* */
/*     <nav id="divPaging">*/
/*         <ul class="paging">*/
/*             {% if startPage>=4 %}*/
/*             <li><a class="btn-info" href="{{ path('All',{'id':startPage-4}) }}"> Page {{ startPage/4+0.75-1}} </a></li>*/
/*              {% endif %}*/
/*             <span>Page {{ startPage/4+0.75}} </span>*/
/*             {% if startPage+4 <= count  %}*/
/*             <li><a class="btn-info" href="{{ path('All',{'id':startPage+4}) }}"> Page {{ startPage/4+1.75}} </a></li>*/
/*             {% endif %}*/
/*         </ul>*/
/*     </nav>*/
/* {% endblock %}*/
/* */
/* */
/* */
