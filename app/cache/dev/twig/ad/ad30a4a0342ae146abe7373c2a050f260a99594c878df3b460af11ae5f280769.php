<?php

/* @CvutFitBiWT1BlogUiBundle/Resources/views/Post/comments.html.twig */
class __TwigTemplate_7d06ec3f3c5791263652fa1574647b85a3e266481a90366db4738d12c9834e79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45daea4fc559a1cd34381d2d2704b20c531e5c0c6c8130c1bab6487958b5dbf8 = $this->env->getExtension("native_profiler");
        $__internal_45daea4fc559a1cd34381d2d2704b20c531e5c0c6c8130c1bab6487958b5dbf8->enter($__internal_45daea4fc559a1cd34381d2d2704b20c531e5c0c6c8130c1bab6487958b5dbf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@CvutFitBiWT1BlogUiBundle/Resources/views/Post/comments.html.twig"));

        // line 1
        if ( !$this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "spam", array())) {
            // line 2
            echo "

    <article>
        <p class=\"comment_par\"><span class=\"author\">";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "getAuthorName", array()), "html", null, true);
            echo " </span> on <time>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "created", array()), (isset($context["date_format"]) ? $context["date_format"] : $this->getContext($context, "date_format"))), "html", null, true);
            echo "</time> said:</p>
        <p class=\"bg-info\">";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "text", array()), "html", null, true);
            echo "</p>
    ";
            // line 7
            if ($this->env->getExtension('security')->isGranted("edit", (isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")))) {
                // line 8
                echo "        <a href=\"/comment/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "id", array()), "html", null, true);
                echo "/delete\" class=\"btn-info\">Delete comment</a>
        <a href=\"/updateComment/";
                // line 9
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "id", array()), "html", null, true);
                echo "\" class=\"btn-info\">Edit comment</a>
        ";
            }
            // line 11
            echo "        <hr>
    </article>



        <span>";
            // line 16
            if ($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "parent", array())) {
                // line 17
                echo "        <a href=\"#";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "parent", array()), "id", array()), "html", null, true);
                echo "\" >@";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "parent", array()), "author", array()), "name", array()), "html", null, true);
                echo "</a>,
        ";
            }
            // line 18
            echo " </span>


    ";
            // line 21
            if ($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "children", array())) {
                // line 22
                echo "        <ul>
            ";
                // line 23
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "children", array()));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 24
                    echo "                ";
                    $this->loadTemplate("@CvutFitBiWT1BlogUiBundle/Resources/views/Post/comments.html.twig", "@CvutFitBiWT1BlogUiBundle/Resources/views/Post/comments.html.twig", 24)->display(array_merge($context, array("comment" => $context["child"])));
                    // line 25
                    echo "            ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 26
                echo "        </ul>
    ";
            }
            // line 28
            echo "
";
        }
        
        $__internal_45daea4fc559a1cd34381d2d2704b20c531e5c0c6c8130c1bab6487958b5dbf8->leave($__internal_45daea4fc559a1cd34381d2d2704b20c531e5c0c6c8130c1bab6487958b5dbf8_prof);

    }

    public function getTemplateName()
    {
        return "@CvutFitBiWT1BlogUiBundle/Resources/views/Post/comments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 28,  112 => 26,  98 => 25,  95 => 24,  78 => 23,  75 => 22,  73 => 21,  68 => 18,  60 => 17,  58 => 16,  51 => 11,  46 => 9,  41 => 8,  39 => 7,  35 => 6,  29 => 5,  24 => 2,  22 => 1,);
    }
}
/* {% if not comment.spam %}*/
/* */
/* */
/*     <article>*/
/*         <p class="comment_par"><span class="author">{{ comment.getAuthorName }} </span> on <time>{{ comment.created|date(date_format) }}</time> said:</p>*/
/*         <p class="bg-info">{{ comment.text }}</p>*/
/*     {% if is_granted('edit',post) %}*/
/*         <a href="/comment/{{ comment.id }}/delete" class="btn-info">Delete comment</a>*/
/*         <a href="/updateComment/{{ comment.id }}" class="btn-info">Edit comment</a>*/
/*         {% endif %}*/
/*         <hr>*/
/*     </article>*/
/* */
/* */
/* */
/*         <span>{% if comment.parent %}*/
/*         <a href="#{{ comment.parent.id }}" >@{{ comment.parent.author.name }}</a>,*/
/*         {% endif %} </span>*/
/* */
/* */
/*     {% if comment.children %}*/
/*         <ul>*/
/*             {% for child in comment.children %}*/
/*                 {% include "@CvutFitBiWT1BlogUiBundle/Resources/views/Post/comments.html.twig" with {'comment': child}%}*/
/*             {% endfor %}*/
/*         </ul>*/
/*     {% endif %}*/
/* */
/* {% endif %}*/
