<?php

/* CvutFitBiWT1BlogUiBundle:Post:new.html.twig */
class __TwigTemplate_a4a157ae663405fcb49f9153ff46a09e5bd948a60139e32717393a8ee8826fb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CvutFitBiWT1BlogUiBundle::base.html.twig", "CvutFitBiWT1BlogUiBundle:Post:new.html.twig", 1);
        $this->blocks = array(
            'page_name' => array($this, 'block_page_name'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CvutFitBiWT1BlogUiBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c35a853092da2457242e8ba2e4c5b1bf960b7ee15921efa60c3b9199c212138d = $this->env->getExtension("native_profiler");
        $__internal_c35a853092da2457242e8ba2e4c5b1bf960b7ee15921efa60c3b9199c212138d->enter($__internal_c35a853092da2457242e8ba2e4c5b1bf960b7ee15921efa60c3b9199c212138d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CvutFitBiWT1BlogUiBundle:Post:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c35a853092da2457242e8ba2e4c5b1bf960b7ee15921efa60c3b9199c212138d->leave($__internal_c35a853092da2457242e8ba2e4c5b1bf960b7ee15921efa60c3b9199c212138d_prof);

    }

    // line 3
    public function block_page_name($context, array $blocks = array())
    {
        $__internal_ba016ca09bbcebddc5df83f195a0abef3d169c1ce4952a47084b89a4b81af13f = $this->env->getExtension("native_profiler");
        $__internal_ba016ca09bbcebddc5df83f195a0abef3d169c1ce4952a47084b89a4b81af13f->enter($__internal_ba016ca09bbcebddc5df83f195a0abef3d169c1ce4952a47084b89a4b81af13f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_name"));

        echo "Nový příspěvek";
        
        $__internal_ba016ca09bbcebddc5df83f195a0abef3d169c1ce4952a47084b89a4b81af13f->leave($__internal_ba016ca09bbcebddc5df83f195a0abef3d169c1ce4952a47084b89a4b81af13f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_145188f22e9236c66deb44b51681eb7071b167e62da48ebdbefb83f841b0a95c = $this->env->getExtension("native_profiler");
        $__internal_145188f22e9236c66deb44b51681eb7071b167e62da48ebdbefb83f841b0a95c->enter($__internal_145188f22e9236c66deb44b51681eb7071b167e62da48ebdbefb83f841b0a95c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <script>
        function ready() {

            var errors = document.getElementsByClassName('has-error');

            if (errors.length!=0) {

                var elements=document.getElementsByClassName('help-block');

                for (var i = 0; i < elements.length; i++) {

                    alert(elements[i].textContent);
                }

            }

            var dangers=document.getElementsByClassName(\"alert-danger\");

            if (dangers.length!=0) {

                    alert(dangers[0].textContent);


            }
        }

        document.addEventListener(\"DOMContentLoaded\", ready);
    </script>


    ";
        // line 37
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

    ";
        // line 41
        echo "
    ";
        // line 42
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "




";
        
        $__internal_145188f22e9236c66deb44b51681eb7071b167e62da48ebdbefb83f841b0a95c->leave($__internal_145188f22e9236c66deb44b51681eb7071b167e62da48ebdbefb83f841b0a95c_prof);

    }

    public function getTemplateName()
    {
        return "CvutFitBiWT1BlogUiBundle:Post:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 42,  95 => 41,  90 => 38,  86 => 37,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'CvutFitBiWT1BlogUiBundle::base.html.twig' %}*/
/* */
/* {% block page_name %}Nový příspěvek{% endblock %}*/
/* */
/* {% block body %}*/
/* */
/*     <script>*/
/*         function ready() {*/
/* */
/*             var errors = document.getElementsByClassName('has-error');*/
/* */
/*             if (errors.length!=0) {*/
/* */
/*                 var elements=document.getElementsByClassName('help-block');*/
/* */
/*                 for (var i = 0; i < elements.length; i++) {*/
/* */
/*                     alert(elements[i].textContent);*/
/*                 }*/
/* */
/*             }*/
/* */
/*             var dangers=document.getElementsByClassName("alert-danger");*/
/* */
/*             if (dangers.length!=0) {*/
/* */
/*                     alert(dangers[0].textContent);*/
/* */
/* */
/*             }*/
/*         }*/
/* */
/*         document.addEventListener("DOMContentLoaded", ready);*/
/*     </script>*/
/* */
/* */
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/* */
/*     {#{{ form_row(form.file) }}#}*/
/* */
/*     {{ form_end(form) }}*/
/* */
/* */
/* */
/* */
/* {% endblock %}*/
