<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/css/0e28421')) {
            // _assetic_0e28421
            if ($pathinfo === '/css/0e28421.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '0e28421',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_0e28421',);
            }

            if (0 === strpos($pathinfo, '/css/0e28421_')) {
                // _assetic_0e28421_0
                if ($pathinfo === '/css/0e28421_normalize_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0e28421',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_0e28421_0',);
                }

                // _assetic_0e28421_1
                if ($pathinfo === '/css/0e28421_main_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0e28421',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_0e28421_1',);
                }

                // _assetic_0e28421_2
                if ($pathinfo === '/css/0e28421_bootstrap.min_3.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0e28421',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_0e28421_2',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/0a79fba')) {
            // _assetic_0a79fba
            if ($pathinfo === '/js/0a79fba.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '0a79fba',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_0a79fba',);
            }

            // _assetic_0a79fba_0
            if ($pathinfo === '/js/0a79fba_skript2_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '0a79fba',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_0a79fba_0',);
            }

        }

        if (0 === strpos($pathinfo, '/images')) {
            if (0 === strpos($pathinfo, '/images/faf7dd2')) {
                // _assetic_faf7dd2
                if ($pathinfo === '/images/faf7dd2.png') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'faf7dd2',  'pos' => NULL,  '_format' => 'png',  '_route' => '_assetic_faf7dd2',);
                }

                // _assetic_faf7dd2_0
                if ($pathinfo === '/images/faf7dd2_ll_1.png') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'faf7dd2',  'pos' => 0,  '_format' => 'png',  '_route' => '_assetic_faf7dd2_0',);
                }

            }

            if (0 === strpos($pathinfo, '/images/0a9f304')) {
                // _assetic_0a9f304
                if ($pathinfo === '/images/0a9f304.png') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0a9f304',  'pos' => NULL,  '_format' => 'png',  '_route' => '_assetic_0a9f304',);
                }

                // _assetic_0a9f304_0
                if ($pathinfo === '/images/0a9f304_login_1.png') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0a9f304',  'pos' => 0,  '_format' => 'png',  '_route' => '_assetic_0a9f304_0',);
                }

            }

            if (0 === strpos($pathinfo, '/images/a1ed59a')) {
                // _assetic_a1ed59a
                if ($pathinfo === '/images/a1ed59a.png') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'a1ed59a',  'pos' => NULL,  '_format' => 'png',  '_route' => '_assetic_a1ed59a',);
                }

                // _assetic_a1ed59a_0
                if ($pathinfo === '/images/a1ed59a_logout_1.png') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'a1ed59a',  'pos' => 0,  '_format' => 'png',  '_route' => '_assetic_a1ed59a_0',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/08ef020')) {
            // _assetic_08ef020
            if ($pathinfo === '/js/08ef020.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '08ef020',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_08ef020',);
            }

            if (0 === strpos($pathinfo, '/js/08ef020_')) {
                // _assetic_08ef020_0
                if ($pathinfo === '/js/08ef020_app_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '08ef020',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_08ef020_0',);
                }

                // _assetic_08ef020_1
                if ($pathinfo === '/js/08ef020_controller_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '08ef020',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_08ef020_1',);
                }

                // _assetic_08ef020_2
                if ($pathinfo === '/js/08ef020_services_3.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '08ef020',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_08ef020_2',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/images')) {
            if (0 === strpos($pathinfo, '/images/d605aa2')) {
                // _assetic_d605aa2
                if ($pathinfo === '/images/d605aa2.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'd605aa2',  'pos' => NULL,  '_format' => 'jpg',  '_route' => '_assetic_d605aa2',);
                }

                // _assetic_d605aa2_0
                if ($pathinfo === '/images/d605aa2_trending2_1.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'd605aa2',  'pos' => 0,  '_format' => 'jpg',  '_route' => '_assetic_d605aa2_0',);
                }

            }

            if (0 === strpos($pathinfo, '/images/a15f5fe')) {
                // _assetic_a15f5fe
                if ($pathinfo === '/images/a15f5fe.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'a15f5fe',  'pos' => NULL,  '_format' => 'jpg',  '_route' => '_assetic_a15f5fe',);
                }

                // _assetic_a15f5fe_0
                if ($pathinfo === '/images/a15f5fe_politics2_1.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'a15f5fe',  'pos' => 0,  '_format' => 'jpg',  '_route' => '_assetic_a15f5fe_0',);
                }

            }

            if (0 === strpos($pathinfo, '/images/1634b78')) {
                // _assetic_1634b78
                if ($pathinfo === '/images/1634b78.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '1634b78',  'pos' => NULL,  '_format' => 'jpg',  '_route' => '_assetic_1634b78',);
                }

                // _assetic_1634b78_0
                if ($pathinfo === '/images/1634b78_life2_1.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '1634b78',  'pos' => 0,  '_format' => 'jpg',  '_route' => '_assetic_1634b78_0',);
                }

            }

            if (0 === strpos($pathinfo, '/images/01cde76')) {
                // _assetic_01cde76
                if ($pathinfo === '/images/01cde76.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '01cde76',  'pos' => NULL,  '_format' => 'jpg',  '_route' => '_assetic_01cde76',);
                }

                // _assetic_01cde76_0
                if ($pathinfo === '/images/01cde76_sport2_1.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '01cde76',  'pos' => 0,  '_format' => 'jpg',  '_route' => '_assetic_01cde76_0',);
                }

            }

            if (0 === strpos($pathinfo, '/images/bb996c9')) {
                // _assetic_bb996c9
                if ($pathinfo === '/images/bb996c9.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'bb996c9',  'pos' => NULL,  '_format' => 'jpg',  '_route' => '_assetic_bb996c9',);
                }

                // _assetic_bb996c9_0
                if ($pathinfo === '/images/bb996c9_tech2_1.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'bb996c9',  'pos' => 0,  '_format' => 'jpg',  '_route' => '_assetic_bb996c9_0',);
                }

            }

            if (0 === strpos($pathinfo, '/images/075f250')) {
                // _assetic_075f250
                if ($pathinfo === '/images/075f250.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '075f250',  'pos' => NULL,  '_format' => 'jpg',  '_route' => '_assetic_075f250',);
                }

                // _assetic_075f250_0
                if ($pathinfo === '/images/075f250_all_1.jpg') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '075f250',  'pos' => 0,  '_format' => 'jpg',  '_route' => '_assetic_075f250_0',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/fd72976')) {
            // _assetic_fd72976
            if ($pathinfo === '/js/fd72976.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fd72976',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_fd72976',);
            }

            // _assetic_fd72976_0
            if ($pathinfo === '/js/fd72976_skript_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fd72976',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_fd72976_0',);
            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // file
        if ($pathinfo === '/file') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::fileAction',  '_route' => 'file',);
        }

        // tmp
        if ($pathinfo === '/tmp') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::tmpAction',  '_route' => 'tmp',);
        }

        if (0 === strpos($pathinfo, '/angular')) {
            // angular
            if ($pathinfo === '/angular') {
                return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::angularAction',  '_route' => 'angular',);
            }

            // angular_detail
            if (0 === strpos($pathinfo, '/angular_detail') && preg_match('#^/angular_detail/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'angular_detail')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::angularDetailAction',));
            }

        }

        // tmp_detail
        if (0 === strpos($pathinfo, '/tmp_detail') && preg_match('#^/tmp_detail/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tmp_detail')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::tmpDetailAction',));
        }

        // new
        if ($pathinfo === '/new') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::newAction',  '_route' => 'new',);
        }

        // detail
        if (0 === strpos($pathinfo, '/post') && preg_match('#^/post/(?P<id>[^/]++)/detail$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'detail')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::detailAction',));
        }

        if (0 === strpos($pathinfo, '/update')) {
            // update
            if (preg_match('#^/update/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'update')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::updateAction',));
            }

            // updateComment
            if (0 === strpos($pathinfo, '/updateComment') && preg_match('#^/updateComment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'updateComment')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::updateCommentAction',));
            }

        }

        // author
        if ($pathinfo === '/author') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::authorAction',  '_route' => 'author',);
        }

        // home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home');
            }

            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::indexAction',  '_route' => 'home',);
        }

        // politics
        if ($pathinfo === '/politics') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::politicsAction',  '_route' => 'politics',);
        }

        if (0 === strpos($pathinfo, '/t')) {
            // trending
            if ($pathinfo === '/trending') {
                return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::trendingAction',  '_route' => 'trending',);
            }

            // tech
            if ($pathinfo === '/tech') {
                return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::techAction',  '_route' => 'tech',);
            }

        }

        // sport
        if ($pathinfo === '/sport') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::sportAction',  '_route' => 'sport',);
        }

        // life
        if ($pathinfo === '/life') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::lifeAction',  '_route' => 'life',);
        }

        // All
        if ($pathinfo === '/All') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::allAction',  '_route' => 'All',);
        }

        // getByAuthor
        if ($pathinfo === '/getByAuthor') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::getByAuthorAction',  '_route' => 'getByAuthor',);
        }

        // delete
        if (0 === strpos($pathinfo, '/post') && preg_match('#^/post/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::deleteAction',));
        }

        // deleteComment
        if (0 === strpos($pathinfo, '/comment') && preg_match('#^/comment/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'deleteComment')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::deleteComment',));
        }

        // detail_page
        if (0 === strpos($pathinfo, '/post') && preg_match('#^/post/(?P<id>[^/]++)/detail/page/(?P<page>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'detail_page')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::detailPageAction',));
        }

        // date
        if ($pathinfo === '/date') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::dateAction',  '_route' => 'date',);
        }

        // tag
        if ($pathinfo === '/tag') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::tagAction',  '_route' => 'tag',);
        }

        if (0 === strpos($pathinfo, '/p')) {
            // private
            if ($pathinfo === '/private') {
                return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\PostController::privateAction',  '_route' => 'private',);
            }

            if (0 === strpos($pathinfo, '/post-')) {
                // post-list.html
                if ($pathinfo === '/post-list.html') {
                    return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController::templateAction',  'template' => '@CvutFitBiWT1BlogUiBundle/Resources/views/Angular/post-list.html',  '_route' => 'post-list.html',);
                }

                // post-detail.html
                if ($pathinfo === '/post-detail.html') {
                    return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController::templateAction',  'template' => '@CvutFitBiWT1BlogUiBundle/Resources/views/Angular/post-detail.html',  '_route' => 'post-detail.html',);
                }

                // post-short.html
                if ($pathinfo === '/post-short.html') {
                    return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController::templateAction',  'template' => '@CvutFitBiWT1BlogUiBundle/Resources/views/Angular/directives/post-short.html',  '_route' => 'post-short.html',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/login')) {
            // login
            if ($pathinfo === '/login') {
                return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
            }

            // login_check
            if ($pathinfo === '/login_check') {
                return array('_route' => 'login_check');
            }

        }

        // user_registration
        if ($pathinfo === '/register') {
            return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\UiBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'user_registration',);
        }

        // logout
        if ($pathinfo === '/logout') {
            return array('_route' => 'logout');
        }

        if (0 === strpos($pathinfo, '/api')) {
            if (0 === strpos($pathinfo, '/api/get_')) {
                if (0 === strpos($pathinfo, '/api/get_post')) {
                    // get_posts_all
                    if ($pathinfo === '/api/get_posts_all') {
                        return array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\ApiBundle\\Controller\\PostController::getAllPosts',  '_route' => 'get_posts_all',);
                    }

                    // get_post
                    if (preg_match('#^/api/get_post/(?P<id>[^/\\.]++)\\.(?P<_format>json|xml)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_post')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\ApiBundle\\Controller\\PostController::getOnePost',));
                    }

                }

                // get_one
                if (0 === strpos($pathinfo, '/api/get_one') && preg_match('#^/api/get_one/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_one')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\ApiBundle\\Controller\\PostController::getOne',));
                }

            }

            // get_all
            if (0 === strpos($pathinfo, '/api/all') && preg_match('#^/api/all\\.(?P<_format>json|xml)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_all')), array (  '_controller' => 'Cvut\\Fit\\BiWT1\\Blog\\ApiBundle\\Controller\\PostController::getAllPost',));
            }

            // nelmio_api_doc_index
            if (0 === strpos($pathinfo, '/api/doc') && preg_match('#^/api/doc(?:/(?P<view>[^/]++))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_nelmio_api_doc_index;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'nelmio_api_doc_index')), array (  '_controller' => 'Nelmio\\ApiDocBundle\\Controller\\ApiDocController::indexAction',  'view' => 'default',));
            }
            not_nelmio_api_doc_index:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
