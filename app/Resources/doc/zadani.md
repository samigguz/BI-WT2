# BI-WT1 - Semestrální úloha - Blog

## Zadání

Implementujte webovou aplikaci Blog podle následujících požadavků:
  * Při implementaci datového modelu zohledněte [diagram](object_model.png).
  * Při implementaci back-endu použijte model "Repository - Functionality - Operation".
  * Při implementaci back-endu i front-endu zohledněte [požadavky](pozadavky.md).

## Termín hodnocení

Termín automatického vyhodnocení bude doplněn. 

## Způsob hodnocení

Semestrální práce bude hodnocena automatizovanými testy.

Práce bude hodnocena 0 body v případě, že:
  * nejsou splněny nefunkční požadavky uvedené v zadání nebo kód obsahuje neošetřené chyby.
  * byla odevzdána po termínu (neplatí v odůvodněných případech - např. nemoc).
  * z logu nástroje Git není patrná průběžná práce (tj. log bude obsahovat minimum záznamů) - posouzení přísluší osobě vyučujícího!
  * z logu nástroje Git není možné jasně identifikovat osobu, která prováděla většinu změn - za vhodnou identifikaci jsou považovány jméno, příjmení a fakultní e-mailová adresa.

## Postup vyhodnocení
  1. Z vašeho repozitáře bude staženo vaše řešení.
  1. Budou porovnány třídy testů ve vašem projektu a v zadání.
  1. Budou spuštěny testy a vypočítána první část dílčího hodnocení.
  1. Podle míry splnění funkčních požadavků bude vypočítána druhá číst dílčího hodnocení. 
