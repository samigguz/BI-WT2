/**
 * Created by Guzel on 28.04.2016.
 */


var postsApp=angular.module('postsApp', ['ngRoute']);

postsApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/angular', {
                templateUrl: 'post-list.html',
                controller: 'PostController'
            }).
// TODO: doplnit routu pro detail prispevku
            otherwise({
                redirectTo: '/angular'
            });
    }]);


postsApp.directive('postShort', function() {
    return {
        templateUrl: '../views/Angular/directives/post-short.html'
    };
});


