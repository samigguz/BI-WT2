/**
 * Created by Guzel on 05.05.2016.
 */


var postControllers = angular.module('postControllers', []);
/*

postControllers.controller('PostController',['$scope', '$http',
    function ($scope, $http) {
        $http.get("/api/all.json").success(function(data) {
            $scope.postList = data;
        });

    }]);*/

/*postControllers.controller('PostDetailController', ['$scope', '$routeParams', '$http',
 function($scope, $routeParams, $http) {

 $http.get("/api/get_one/"+$routeParams.postId).success(function(data) {
 $scope.post_one = data;
 });
 }]);
 */
postControllers.controller('PostController',['$scope', 'all_posts',
    function ($scope,all_posts) {

            $scope.postList = all_posts.query();
    }]);


postControllers.controller('PostDetailController', ['$scope', '$routeParams', 'one_post',
 function($scope, $routeParams, one_post) {

     $scope.post_one = one_post.query({id:$routeParams.postId});

 }]);
postControllers.controller('ExampleController', ['$scope','one_post', function($scope,one_post) {
    
    
    $scope.master = {};

    $scope.edit=function () {
        
        $scope.formular.visibility="visible";

    }
    $scope.reset = function() {
        $scope.post = angular.copy($scope.master);
    };

    
    $scope.submit=function (post_one) {

        $scope.myStyle2={visibility: 'visible'};
        $scope.myStyle={visibility: 'hidden'};
        $scope.master = angular.copy(post_one);

        $scope.show_detail=true;
        $scope.entry = new one_post();

        one_post.save($scope.master, function() {});

        one_post.update({ id: post_one.id}, post_one);
    }
}]);

