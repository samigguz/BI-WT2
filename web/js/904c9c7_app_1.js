/**
 * Created by Guzel on 28.04.2016.
 */

app.directive('postShort', function() {
    return {
        templateUrl: 'templates/directives/post-short.html'
    };
});

angular.module('postsApp', [])
    .controller('PostController', function($http) {

        var postList = this;
        var tags;
        $http.get("/api/all.json").then(function(response) {
            postList.list = response.data;
            });

    });