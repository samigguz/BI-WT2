/**
 * Created by guzel on 12.05.16.
 */


var restServices=angular.module('restServices', ['ngResource']);

restServices.factory('all_posts',['$resource',
    function($resource) {
        return  $resource('/api/all.json', {},{query: {method:'GET',isArray: true}})

    }]);

restServices.factory('one_post',['$resource',
    function($resource) {
        return  $resource('/api/get_one/:id', {id: '@id'},
            {
                 query:  {method:'GET',isArray: false},
                 update: { method:'PUT'}
            })

    }]);

///api/all.json