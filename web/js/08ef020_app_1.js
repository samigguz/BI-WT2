/**
 * Created by Guzel on 28.04.2016.
 */


var postsApp=angular.module('postsApp', ['ngRoute','postControllers','restServices']);

postsApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/posts', {
                templateUrl: 'post-list.html',
                controller: 'PostController'
            }).
            when('/post/:postId', {
                templateUrl: 'post-detail.html',
                controller: 'PostDetailController'
            }).
            otherwise({
                redirectTo: '/angular'
            });
    }]);



postsApp.directive('postShort', function() {
    return {
        templateUrl: 'post-short.html'
    };
});


