<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 18.03.2016
 * Time: 10:23
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Serializer;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Operation\PostOperation;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\PostRepository;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\PostFunctionality;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Tests\Controller;
use GuzzleHttp\Psr7;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;

class PostSerializer extends Container{

   protected $serializer;

    function setSerializer($serializer){

        $this->serializer=$serializer;
    }

    function deserialize($data,$url,$format,$context){

        $object = $this->serializer->deserialize($data, $url, $format);
        $items=$object->getChannel()->getItem();

        for($i=0;$i<count($items);$i++) {


            $item = $items[$i];
            $title = $item->getTitle();
            $text = $item->getDescription();
            $username = $item->getAuthor();
            $link = $item->getLink();
            $pubdate = $item->getPubDate();


            $post = new Post();
            $author = new User();
            $author->setName($username);
            $author->setPassword("password".$i);
            $post->setAuthor($author);
            $post->setText($link." ".$text);
            $post->setTitle($title);
            $post->setCreated($pubdate);
            $post->setModified(new \DateTime());
            $post->setPublishFrom(new \DateTime());
            $post->setPublishTo((new \DateTime()));

            $context->create($post,array(),array());
        }



    }

    function serialize($post,$format){

       return $this->serializer->serialize($post, $format);

    }

}