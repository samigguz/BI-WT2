<?php
/**
 * Created by PhpStorm.
 * User: jirkovoj
 * Date: 24/11/15
 * Time: 18:56
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Operation;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Comment;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\CommentFunctionality;



class CommentOperation
{
	/** @var CommentFunctionality */
	protected $commentFunctionality;

	public function __construct()
	{}

	/**
	 * @param CommentFunctionality $commentFunctionality
	 */
	public function setCommentFunctionality($commentFunctionality)
	{
		$this->commentFunctionality = $commentFunctionality;
	}

	public function create(Comment $comment)
	{
		$comment->setCreated(new \DateTime);
		$comment->setModified(new \DateTime);
		$this->commentFunctionality->create($comment);
	}

	public function update(Comment $comment){
		$comment->setModified(new \DateTime);
		$this->commentFunctionality->update($comment);
	}

	public function delete(Comment $comment){
		$this->commentFunctionality->delete($comment);
	}

	public function getById($id){
		return $this->commentFunctionality->findById($id);
	}

	public function getByPost($page, $post){
		return $this->commentFunctionality->getByPage($page, $post);
	}
}