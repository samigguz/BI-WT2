<?php
/**
 * Created by PhpStorm.
 * User: jirkovoj
 * Date: 24/11/15
 * Time: 18:56
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Operation;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Tag;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\File;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\FileFunctionality;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\PostFunctionality;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\TagFunctionality;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\CommentFunctionality;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\UserFunctionality;

class PostOperation
{
	/** @var UserFunctionality */
	protected $userFunctionality;

	/**
	 * @param UserFunctionality $userFunctionality
	 */
	public function setUserFunctionality($userFunctionality)
	{
		$this->userFunctionality = $userFunctionality;
	}
	/** @var PostFunctionality */
	protected $postFunctionality;
	public function __construct() {}

	/**
	 * @param PostFunctionality $postFunctionality
	 */

	public function setPostFunctionality($postFunctionality)
	{
		$this->postFunctionality = $postFunctionality;
	}

	/** @var TagFunctionality */
	protected $tagFunctionality;
	/**
	 * @param TagFunctionality $tagFunctionality
	 */

	public function setTagFunctionality($tagFunctionality)
	{
		$this->tagFunctionality = $tagFunctionality;
	}

	/** @var FileFunctionality */
	protected $fileFunctionality;

	/**
	 * @param FileFunctionality $fileFunctionality
	 */

	public function setFileFunctionality($fileFunctionality)
	{
		$this->fileFunctionality = $fileFunctionality;
	}

	/** @var CommentFunctionality */
	protected $commentFunctionality;
	/**
	 * @param CommentFunctionality $commentFunctionality
	 */
	public function setCommentFunctionality($commentFunctionality)
	{
		$this->commentFunctionality = $commentFunctionality;
	}
	/**
	 * @param Post $post
	 * @param array $files
	 * @param array $tags
	 */
	public function create(Post $post, $files, $tags)
	{


		foreach($files as $key=>$value) {
			$a=new File();
			$a->setName($key);
			$a->setCreated(new \DateTime());
			$a->setInternetMediaType('file');
			$a->setData($value);
			$post->addFile($a);
		}

		foreach($tags as $value) {

			$a = new Tag();
			$a->setTitle($value);
			$a=$this->tagFunctionality->create($a);
			$post->addTag($a);

		}


		$this->postFunctionality->create($post);


	}
	public function update(Post $post, $files, $tags)
	{


		foreach($files as $key=>$value) {
			$a=new File();
			$a->setName($key);
			$a->setCreated(new \DateTime());
			$a->setInternetMediaType('file');
			$a->setData($value);
			$post->addFile($a);
		}

		foreach($tags as $value) {

			$a = new Tag();
			$a->setTitle($value);
			$a=$this->tagFunctionality->create($a);
			$post->addTag($a);

		}


		$this->postFunctionality->update($post);


	}
	public function createComment($comment)
	{
		$this->commentFunctionality->create($comment);
	}
	public function updateComment($comment)
	{
		$this->commentFunctionality->update($comment);
	}
	public function findAll()
	{
		return $this->postFunctionality->findAll();
	}
	public function findById($id)
	{
		return $this->postFunctionality->findById($id);
	}
	public function removePost($post)
	{
        $this->postFunctionality->delete($post);
	}
	public function removeComment($comment)
	{
		$this->commentFunctionality->delete($comment);
	}
	public function getTags()
	{
		return $this->tagFunctionality->findAll();
	}
	public function getComments()
	{
		return $this->commentFunctionality->findAll();
	}
	public function findByTitle($title)
	{
		return $this->postFunctionality->findByTitle($title);
	}
	public function findByCommentId($id)
	{
		return $this->commentFunctionality->findById($id);
	}
	public function findPostByCommentId($id)
	{
		return $this->commentFunctionality->findPostByCommentId($id);
	}

	/**
	 * @return int
     */
	public function getCount()
	{
		return $this->postFunctionality->findAll()->count();
	}
	public function findByAuthor($author)
	{
		return $this->postFunctionality->findByAuthor($author);
	}
	public function getAllUsers(){
		return $this->userFunctionality->findAll();
	}
}