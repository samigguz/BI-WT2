<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 11/18/15
 * Time: 10:35 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Exception\ItemNotFoundException;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Tag;

class PostFunctionality
{
    /** @var PostRepository */
    protected $postRepository;

    /**
     * @param PostRepository $postRepository
     */
    public function setPostRepository($postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param Post $post
     * @return Post
     */
    public function create(Post $post)
    {
        $post->setCreated(new \DateTime);
        $this->postRepository->save($post);
        return $post;
    }

    /**
     * @param Post $post
     * @return Post
     * @throws ItemNotFoundException
     */
    public function update(Post $post)
    {
        try {
            $post->setModified(new \DateTime);
            $this->postRepository->save($post);
            return $post;
        } catch (ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param Post $post
     * @return Post
     * @throws ItemNotFoundException
     */
    public function delete(Post $post)
    {
        try {
            $this->postRepository->delete($post);
            return $post;
        } catch (ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return Post
     * @throws ItemNotFoundException
     */
    public function findById($id)
    {
        $post = $this->postRepository->findById($id);
        if ($post instanceof Post)
            return $post;

        throw new ItemNotFoundException();
    }

    /**
     * @return Collection<Post>
     */
    public function findAll()
    {
        return $this->postRepository->findAll();
    }

    /**
     * @param Tag $tag
     * @return Collection<Post>
     */
    public function findByTag(Tag $tag)
    {
        $result = new ArrayCollection();
        $posts = $this->findAll();

        /** @var Post $post */
        foreach ($posts as $post) {
            if ($post->getTags()->contains($tag)) {
                $result->add($post);
            }
        }
        return $result;
    }

    /**
     * @param Collection $users
     * @return Collection<Post>
     */
    public function findByAuthor($users)
    {
        //return new ArrayCollection($this->postRepository->findBy(['author' => $user]));
        $result = new ArrayCollection();
        $posts = $this->findAll();
        foreach ($posts as $post) {
           foreach($users as $user)
           {
               if ($post->getAuthor()->getName()==$user)
                   $result->add($post);
           }

        }
        return $result;
    }


    /**
     * @param string $title
     * @return Collection<Post>
     */
    public function findByTitle($title)
    {
        $result = new ArrayCollection();
        $posts = $this->findAll();


        /** @var Post $post */
        foreach ($posts as $post) {
            $tags=$post->getTags();
            foreach ($tags as $tag) {
            if ($tag->getTitle()== $title) {
                $result->add($post);
            }}
        }
        return $result;
    }
}