<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 11/18/15
 * Time: 10:35 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Exception\ItemNotFoundException;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\UserRepository;

class UserFunctionality
{
    /** @var UserRepository */
    protected $userRepository;

    /**
     * @param userRepository $userRepository
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @return User
     */
    public function create(User $user)
    {

        $this->userRepository->save($user);
        return $user;
    }

    /**
     * @param User $user
     * @return User
     * @throws ItemNotFoundException
     */
    public function update(User $user)
    {
        try {
            $this->userRepository->save($user);
            return $user;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param User $user
     * @return User
     * @throws ItemNotFoundException
     */
    public function delete(User $user)
    {
        try {
            $this->userRepository->delete($user);
            return $user;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return User
     * @throws ItemNotFoundException
     */
    public function findById($id)
    {
        $user = $this->userRepository->findById($id);
        if($user instanceof User)
            return $user;

        throw new ItemNotFoundException();
    }
    /**
     * @param string $name
     * @return User
     * @throws ItemNotFoundException
     */
    public function findByName($name)
    {
        $user = $this->userRepository->findByName($name);
        if($user instanceof User)
            return $user;

       // throw new ItemNotFoundException();
        return 0;
    }
    /**
     * @param string $key
     * @return User
     * @throws ItemNotFoundException
     */
    public function findByApiKey($key)
    {
        $user = $this->userRepository->findByApiKey($key);
        if($user instanceof User)
            return $user;
        return 0;
    }
    /**
     * @param string $key
     * @return string
     * @throws ItemNotFoundException
     */
    public function findUsernameByApiKey($key)
    {
        $username = $this->userRepository->findUsernameByApiKey($key);

        return $username;
    }

    /**
     * @return Collection<Post>
     */
    public function findAll()
    {
        return $this->userRepository->findAll();
    }


}
