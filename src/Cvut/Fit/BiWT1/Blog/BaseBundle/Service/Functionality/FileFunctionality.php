<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 11/18/15
 * Time: 10:35 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Exception\ItemNotFoundException;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\File;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\FileRepository;

class FileFunctionality
{
    /** @var FileRepository */
    protected $fileRepository;

    /**
     * @param FileRepository $fileRepository
     */
    public function setFileRepository($fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param File $file
     * @return File
     */
    public function create(File $file)
    {
        $file->setCreated(new \DateTime);
        $this->fileRepository->save($file);
        return $file;
    }

    /**
     * @param File $file
     * @return File
     * @throws ItemNotFoundException
     */
    public function update(File $file)
    {
        try {

            $this->fileRepository->save($file);
            return $file;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param File $file
     * @return File
     * @throws ItemNotFoundException
     */
    public function delete(File $file)
    {
        try {
            $this->fileRepository->delete($file);
            return $file;
        } catch(ItemNotFoundException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return File
     * @throws ItemNotFoundException
     */
    public function findById($id)
    {
        $file = $this->fileRepository->findById($id);
        if($file instanceof File)
            return $file;

        throw new ItemNotFoundException();
    }

    /**
     * @return Collection<File>
     */
    public function findAll()
    {
        return $this->fileRepository->findAll();
    }
    /**
     * @param File $file
     * @return string
     */
    public function retrieve(File $file)
    {
        return $this->fileRepository->findById($file->getId())->getData();
    }


}
