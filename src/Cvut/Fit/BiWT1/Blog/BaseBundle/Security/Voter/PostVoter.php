<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 03.03.2016
 * Time: 21:09
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;

class PostVoter extends AbstractVoter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    protected function getSupportedAttributes()
    {
        return array(self::VIEW, self::EDIT);
    }

    public function supportsAttribute($str)
    {
        if (in_array($str,$this->getSupportedAttributes()))
         return true;

        return false;
    }
    protected function getSupportedClasses()
    {
        return array('Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post');
    }
    public function supportsClass($class)
    {
        $supportedClass = 'Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post';

        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
    }



    public function isGranted($attribute, $post, $user = null)
    {
        if ($attribute == "view" && !$post->isPrivate() ){
            return true;
        }

        if ($attribute == "view" && $user){
            return true;
        }

        if (!is_object($user)) {
            return false;
        }

        if( $user->getIsAdmin()) {
            return true;
        }


        if ($attribute == "edit" && $user->getId() === $post->getAuthor()->getId()) {
            return true;
        }

        return false;
    }
}