<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 29.02.2016
 * Time: 22:20
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\UserFunctionality;

class SetPasswordCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('demo:setpassword')
            ->setDescription('Set password')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Name od user'
            )
            ->addArgument(
                'password',
                InputArgument::REQUIRED,
                'Set password'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $text="error";
        $name = $input->getArgument('name');
        if ($name) {
            $text = 'Hello '.$name;
        } else {
            $text = 'Hello';
        }
        $passw = $input->getArgument('password');
        if ($passw) {
            $text = $text." ".$passw;
        } else {
            $text = 'error:no password';
        }
        $container=$this->getContainer();
        $tmp = $container->get('cvut_fit_biwt1_blog_base.service.functionality.user');

        if ($find_user=$tmp->findByName($name))
        {
            $plainPassword = $passw;
            $encoder = $container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($find_user, $plainPassword);
            $find_user->setPassword($encoded);
            $tmp->update($find_user);
        }
        else {

            $user = new User();
            $user->setName($name);
            $plainPassword = $passw;
            $encoder = $container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);
            $tmp->create($user);
        }
        $output->writeln($text." ".$encoded);
    }
}