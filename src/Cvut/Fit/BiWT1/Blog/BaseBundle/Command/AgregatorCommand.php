<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 29.02.2016
 * Time: 22:20
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Command;

use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Serializer\PostSerializer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Operation\PostOperation;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\PostRepository;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Service\Functionality\PostFunctionality;
use Symfony\Component\HttpKernel\Tests\Controller;
use GuzzleHttp\Psr7;

class AgregatorCommand extends ContainerAwareCommand
{

    const ENCODING = 'UTF-8';
    protected $client;
    protected function configure()
    {
        $this
            ->setName('demo:agregator')
            ->setDescription('Agregator')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'Url'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('url');
        if ($name) {
            $output->writeln( 'Your url is  '.$name);

        } else {
            $output->writeln( 'error');
        }


        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $name);
        $data = $response->getBody()->getContents();

        $tmp = $this->getContainer()->get('cvut_fit_biwt1_blog_base.service.serializer.post');

        $tmp->deserialize($data,'Cvut\Fit\BiWT1\Blog\BaseBundle\Rss\Rss','xml', $this->getContainer()->get('cvut_fit_biwt1_blog_base.service.operation.post'));



    }
}