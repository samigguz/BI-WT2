<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 8/18/15
 * Time: 10:04 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class Tag
 *
 * @ORM\Entity(repositoryClass="TagRepository")
 * @ORM\Table(name="Tag")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class Tag
{
    /**
     * Unikatni ID prispevku
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    protected $id;

    /**
     * Titulek prispevku
     *
     * @ORM\Column(type="string")
     * @var string
     */
    protected $title;
    /**
     * 
     * @ORM\ManyToMany(targetEntity="Post",
     *   mappedBy="tags",cascade={"persist"})
     *
     * @var Collection<Post>
     */
    protected $posts;

    /**
     * Tag constructor.
     * @param number $id
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param number $id
     */

        public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Collection $posts
     */
    public function addPost($post) {

        $this->posts->add($post);

        if (!$post->getTags()->contains($this)) {
            $post->addTag($this);
        }
    }


    function removePost($post){

        $this->posts->removeElement($post);

    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

}
