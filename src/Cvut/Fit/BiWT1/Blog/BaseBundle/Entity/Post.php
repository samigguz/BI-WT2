<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 8/18/15
 * Time: 10:04 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping\OrderBy;
/**
 * Class Post
 *
 * @ORM\Entity(repositoryClass="PostRepository")
 * @ORM\Table(name="Post")
 * @package Cvut\Fit\BiWT1\Blog\BaseBundle\Entity
 *
 * @JMS\XmlRoot("Post")
 * @JMS\ExclusionPolicy("all")
 *
 */
class Post
{
    /**
     * Unikatni ID prispevku
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     * @JMS\Expose
     */
    protected $id;

    /**
     * Titulek prispevku
     *
     * @ORM\Column(type="string")
     * @var string
     * @Assert\NotBlank()
     * @JMS\Expose
     */
    protected $title;

    /**
     * Text (obsah) prispevku
     *
     * @ORM\Column(type="string")
     * @var string
     *  @Assert\NotBlank()
     *  @Assert\Length(min = 3)
     * @JMS\Expose
     */
    protected $text;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @JMS\Expose
     */
    protected $author;

    /**
     * Soukromi prispevku
     *
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $private = false;

    /**
     * Datum vytvoreni prispevku
     *
     *
     * @ORM\Column(type="datetime")
     * @var \Datetime
     * @JMS\Expose
     */
    protected $created;

    /**
     * Datum posledni zmeny prispevku
     *
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $modified;

    /**
     * Viditelnost prispevku (od)
     *
     * @ORM\Column(type="datetime")
     * @var \Datetime
     * @Assert\Date()
     * @JMS\Expose
     */
    protected $publishFrom;

    /**
     * Viditelnost prispevku (do)
     *
     * @ORM\Column(type="datetime")
     * @var \Datetime
     * @Assert\Date()
     * @JMS\Expose
     */
    protected $publishTo;

    /**
     * Kolekce komentaru prispevku
     *
     *
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="post",cascade={"remove","merge"})
     * @var Collection<Comment>
     * @JMS\Expose
     */
    protected $comments;
//@ORM\OrderBY({"created" = "ASC"})
    /**
     * Kolekce souboru prispevku
     *
     * @ORM\OneToMany(targetEntity="File", mappedBy="post",cascade={"remove","merge"})
     * @var Collection<File>
     */
    protected $files;
    
    /**
     *
     * @ORM\Column(type="string",nullable=true)
     * @Assert\File(mimeTypes={ "image/*" })
     */
    protected $file=null;

    /**
     * @return file
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param file $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }


    /**
     * Kolekce tagu prispevku
     *
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="posts",cascade={"merge"})
     * @ORM\JoinTable(
     *     name="post_tags",
     *     joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     * })
     * 
     * @var Collection<Tag>
     * @JMS\Expose
     */
    protected $tags;

    function __construct()    {

        $this->comments=new ArrayCollection();
        $this->files=new ArrayCollection();
        $this->tags=new ArrayCollection();
        $this->tagss="hello";
        $this->publishFrom=NULL;
        $this->publishTo=NULL;

    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return \Datetime
     */

    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \Datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return Collection
     */

    public function getComments()
    {
       /* $sort = Criteria::create();
        $sort->orderBy(Array(
            'Order' => Criteria::ASC
        ));
        return $this->comments->matching($sort);*/
        return $this->comments;
    }

    /**
     * @param Collection $comments
     */


    /**
     * @return Collection
     */

    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param Collection $files
     */


    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return boolean
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param boolean $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return \Datetime
     */
    public function getPublishFrom()
    {
        return $this->publishFrom;
    }

    /**
     * @param \Datetime $publishFrom
     */
    public function setPublishFrom($publishFrom)
    {
        $this->publishFrom = $publishFrom;
    }

    /**
     * @return \Datetime
     */
    public function getPublishTo()
    {
        return $this->publishTo;
    }

    /**
     * @param \Datetime $publishTo
     */
    public function setPublishTo($publishTo)
    {
        $this->publishTo = $publishTo;
    }

    /**
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }
    /**
     * @return string
     */
    public function getTag()
    {
        return strtolower($this->tags[0]->getTitle());
    }

    /**
     * @param Collection $tags
     */


    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
    /**
     * @return string
     */
    public function get150Text()
    {

        return substr($this->text, 0, 150) . "...";
    }
    public function getSmallTitle()
    {

        return substr($this->title,0,30)."...";
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    function addComment($comment){
        $this->comments->add($comment);
        $comment->setPost($this);
    }

    function removeComment(&$comment){

       $this->comments->removeElement($comment);
    }

    function addFile(&$file){
        $this->files->add($file);
        $file->setPost($this);

    }
    function removeFile($file){

        $this->files->removeElement($file);

    }
    function addTag($tag){




        $this->tags->add($tag);

        if (!$tag->getPosts()->contains($this)) {
            $tag->addPost($this);

        }

    }
    function removeTag($tag){


        $this->tags->removeElement($tag);

    }
    function showPost(){
        echo  "Nazev: "."$this->title\n";
        echo "Text: "."$this->text\n";
        echo "Komentare:\n";
        foreach ($this->comments as $key=>$value) {

            $value->showComment();
        }
        echo "Soubory:\n";
        foreach ($this->files as $key=>$value){

            $value->showFileDetails();
        }
    }


    /**
     * Get private
     *
     * @return boolean
     */
    public function getPrivate()
    {
        return $this->private;
    }
}
