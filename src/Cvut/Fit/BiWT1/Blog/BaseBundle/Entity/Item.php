<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 17.03.2016
 * Time: 23:36
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use JMS\Serializer\Annotation as JMS;
/**
 * Class Item
 *
 *
 * @JMS\XmlRoot("Item")
 *
 */

class Item
{
    const ENCODING = 'UTF-8';

    /**
     * @JMS\Type("string")
     */
    protected $title;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPubDate()
    {
        return $this->pubDate;
    }

    /**
     * @param mixed $pubDate
     */
    public function setPubDate($pubDate)
    {
        $this->pubDate = $pubDate;
    }

    /**
     *
     * @JMS\Type("string")
     */
    protected $link;

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @JMS\Type("string")
     */
     protected $description;

    /**
     * @JMS\Type("datetime")
     */
    protected $pubDate;


}