<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 8/18/15
 * Time: 10:04 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Post
 *
 * @ORM\Entity(repositoryClass="UserRepository")
 * @ORM\Table(name="User")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class User implements UserInterface
{
    /**
     * Unikatni ID prispevku
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    protected $id;
    /**
     *
     * @ORM\Column(name="apikey",type="string")
     * @var string
     */
    protected $apiKey;
    /**
     * Unikatni jmeno prispevku
     *
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    protected $username;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $password;
    /**
     *
     *
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author")
     *
     * @var Collection<Post>
     */
    protected $posts;
    /**
     * @ORM\Column(name="is_admin", type="boolean")
     */
    protected $isAdmin;


    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @return number
     */

    function __construct()
    {
        $this->posts=new ArrayCollection();
        $this->isActive = true;
        $this->isAdmin = false;
        // $this->passwrd=NULL;
    }
    public function getId()
    {
        return $this->id;
    }
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }


    /**
     * @param string $apiKey
     */
    public function setApiKey($key)
    {
        $this->apiKey= $key;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->username;
    }


    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->username = $name;
    }

    /**
     * @return Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Collection $posts
     */
    function addPost($post) {
        $this->posts->add($post);
        $post->setAuthor($this);

    }
    function removePost($post){

        $this->posts->removeElement($post);
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }


    /**
     * @param string $pas
     */
    public function setPassword($pas)
    {
        $this->password = $pas;
    }

    public function getRoles()
    {
        if ($this->isAdmin) return array('ROLE_ADMIN');
        return array('ROLE_USER');
    }
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($user)
    {
        $this->username=$user;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }
}
