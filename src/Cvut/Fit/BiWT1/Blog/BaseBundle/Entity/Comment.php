<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 8/18/15
 * Time: 10:04 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;



use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class Comment
 *
 * @ORM\Entity(repositoryClass="CommentRepository")
 * @ORM\Table(name="Comment")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class Comment
{
    /**
     * Unikatni ID prispevku
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    protected $id;

    /**
     * Text (obsah) prispevku
     *
     * @ORM\Column(type="string")
     * @var string
     */
    protected $text;

    /**
     * Autor prispevku
     *
     *
     * @ORM\ManyToOne(targetEntity="User",cascade={"merge"})
     * @var User
     */
    protected $author;
    /**
     * Autor prispevku
     *
     * @ORM\Column(type="string")
     * @var string
     */
    protected $author_name;
    /**
     * @ORM\ManyToOne(targetEntity="Post",
     *   inversedBy="comments")
     * @ORM\JoinColumn(name="post",
     *   referencedColumnName="id")
     * @var Post
     */
    protected $post;

    /**
     * Parent prispevku
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @var Comment
     */
    protected $parent;

    /**
     * childrens prispevku
     *
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="parent")
     * @var Collection<Comment>
     */
    protected $children;
    /**
     * Datum vytvoreni prispevku
     *
     * @ORM\Column(type="datetime")
     * @var \Datetime
     */
    protected $created;

    /**
     * Datum posledni zmeny prispevku
     *
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $modified;

    /**
     * Kolekce souboru prispevku
     *
     * @ORM\OneToMany(targetEntity="File", mappedBy="comment",cascade={"remove"})
     * @var Collection<File>
     */
    protected $files;
    /**
     * spam
     *
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $spam = false;


    function __construct()    {


        $this->children=new ArrayCollection();
        $this->files=new ArrayCollection();
        $this->parent=NULL;
        $this->author_name=get_current_user();


    }

    function addFile(&$file) {
        $this->files->add($file);
        $file->setComment($this);
        return $this;
    }
    function removeFile(&$file){

        foreach ($this->files as $key=>$value) {
            if($value == $file)
            {
                unset($this->files[$key]);
            }
        }
        return $this;
    }
    function showComment(){
        echo  "\t"."$this->text"."\n";
        echo  "\t Soubory: \n";
        foreach ($this->files as $key=>$value)
            $value->showFileDetails();
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }
    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->author_name;
    }
    /**
     * @param string $str
     */
    public function setAuthorName($str)
    {
        $this->author_name=$str;
    }
    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Collection $children
     */
    function AddChild(&$child) {
        $this->children->add($child);
        $child->setParent($this);
        return $this;
    }
    function removeChild(&$ch){

        foreach ($this->children as $key=>$value) {
            if($value == $ch)
            {
                unset($this->children[$key]);
            }
        }
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \Datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param Collection $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return Comment
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Comment $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }
    public function getPostId()
    {
        return $this->post->getId();
    }


    /**
     * @param Post $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * @return boolean
     */
    public function isSpam()
    {
        return $this->spam;
    }

    /**
     * @param boolean $spam
     */
    public function setSpam($spam)
    {
        $this->spam = $spam;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }



    /**
     * Get spam
     *
     * @return boolean
     */
    public function getSpam()
    {
        return $this->spam;
    }
}
