<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 8/18/15
 * Time: 10:04 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Image
 *
 * @ORM\Entity(repositoryClass="ImageRepository")
 * @ORM\Table(name="Image")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */

class Image extends File
{

    /**
     * DimensionX
     *
     * @ORM\Column(type="integer")
     * @var integer
     */
   protected $dimensionX;
    /**
     * DimensionY
     *
     * @ORM\Column(type="integer")
     * @var integer
     */
   protected $dimensionY;
    /**
     * Unikatni ID prispevku
     *
     * @ORM\Column(type="blob")
     *
     * @var Blob
     */
    protected $preview;

    /**
     * @return Blob
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param Blob $preview
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;
    }



   function  getDimensionX()              {return $this->dimensionX;}
   function  setDimensionX($dimensionX)   {$this->dimensionX=$dimensionX;}
   function  getDimensionY()              {return $this->dimensionY;}
   function  setDimensionY($dimensionY)   {$this->dimensionY=$dimensionY;}
   function  showFileDetails()            { echo "\t"."$this->name".", "."$this->dimensionX"." x "."$this->dimensionY\n";}

}
