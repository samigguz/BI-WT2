<?php

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 *
 * @ORM\Entity(repositoryClass="FileRepository")
 * @ORM\Table(name="File")
 * @package Cvut\Fit\Ict\Blog_151\BaseBundle\Entity
 */
class File
{
    /**
     * Unikatni ID prispevku
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="files")
     * @ORM\JoinColumn(name="post", referencedColumnName="id")
     * @var Post
     */
    protected $post;


    /**
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="files")
     * @ORM\JoinColumn(name="comment", referencedColumnName="id")
     * @var Post
     */
    protected $comment;

    /**
     * @ORM\Column(type="datetime")
     * @var \Datetime
     */
    protected $created;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $internetMediaType;

    /**
     * @ORM\Column(type="blob")
     * @var Blob $data
     */
    protected $data;

    /**
     * File constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param Post $post
     */
    public function setPost($post)
    {
        if(!$post->getFiles()->contains($this))
            $post->addFile($this);
        $this->post = $post;
    }

    /**
     * @return Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param Comment $comment
     */
    public function setComment($comment)
    {
        if(!$comment->getFiles()->contains($this))
            $comment->addFile($this);
        $this->comment = $comment;
    }

    /**
     * @return Datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param Datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getInternetMediaType()
    {
        return $this->internetMediaType;
    }

    /**
     * @param string $internetMediaType
     */
    public function setInternetMediaType($internetMediaType)
    {
        $this->internetMediaType = $internetMediaType;
    }

    /**
     * @return Blob
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param Blob $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }



}