<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 8/18/15
 * Time: 10:00 AM
 */

namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{


    /**
     * @param int $id
     * @return Post
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @param Post $post
     */
    public function save(Post $post)
    {
        $this->getEntityManager()->merge($post);
        $this->getEntityManager()->flush();
    }

    /**
 * @param Post $post
 */
public function delete(Post $post)
{
    $this->getEntityManager()->remove($post);
    $this->getEntityManager()->flush();
}

/**
 * 
 * @return Collection<Post>
 */
public function findAll()
{
  /*  $sql = 'SELECT * FROM Post WHERE  publish_from <= :date1 ORDER BY created DESC ';
    $date = new \DateTime;
    $params = array(
        'date1'  => $date->format('Y-m-d')
    );
    return  $this->getEntityManager()->getConnection()
        ->executeQuery($sql, $params)
        ->fetchAll(\PDO::FETCH_CLASS, "\\Cvut\\Fit\\BiWT1\\Blog\\BaseBundle\\Entity\\Post");*/
    return new ArrayCollection(parent::findAll());
}
}