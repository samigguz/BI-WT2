<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 17.03.2016
 * Time: 23:36
 */
namespace Cvut\Fit\BiWT1\Blog\BaseBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use JMS\Serializer\Annotation as JMS;

class Channel
{

    const ENCODING = 'UTF-8';
    /**
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     *
     * @JMS\Type("ArrayCollection<Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Item>")
     * @JMS\XmlList(entry="item")
     */
    protected $channel;




    function __construct()
    {
      $this->channel=new ArrayCollection();


    }
    function getChannel(){

        return $this->channel;
    }

}