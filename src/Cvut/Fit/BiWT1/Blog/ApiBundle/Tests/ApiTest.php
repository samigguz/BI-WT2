<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 10.03.2016
 * Time: 23:33
 */

namespace Cvut\Fit\BiWT1\Blog\ApiBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Cookie;


class ApiTest extends WebTestCase
{
    /** @var Client */
    protected $client;

    public function setUp()
    {
        parent::setUp();
        $this->client = static::createClient([], [
            'HTTP_HOST' => 'localhost:8000'
        ]);
    }


    public function testLogin()
    {
        // request
        $crawler = $this->client->request('GET', '/login');


        $form = $crawler->selectButton('login')->form();
        $crawler = $this->client->submit($form, array('_username' => 'Guzel',
            '_password'=>'guzel'
        ));
        $crawler = $this->client->followRedirect();
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        return $this->client;
    }
    // ...
    /**
     * @depends testLogin
     */
    public function testApi($loggedClient = null)
    {
        $this->client = $loggedClient;

        //---------xml-------------

        $crawler = $this->client->request('GET', '/api/get_post/195.xml');

        $status = $this->client->getResponse()->getStatusCode();
        $this->assertEquals(200, $status);

        var_dump($this->client->getResponse()->getContent());

        $id = $crawler->filter('id')->last()->text();
        $this->assertEquals(195, $id);

        $title = $crawler->filter('title')->last()->text();
        $this->assertEquals('MyTitle', $title);

        $text = $crawler->filter('text')->last()->text();
        $this->assertEquals('ItisText', $text);


        //---------json------------

        $crawler = $this->client->request('GET', '/api/get_post/195.json');

        $status = $this->client->getResponse()->getStatusCode();
        $this->assertEquals(200, $status);

        $obj = json_decode($this->client->getResponse()->getContent());

        $id=$obj->{'id'};

        $this->assertEquals(195, $id);

        $title=$obj->{'title'};

        $this->assertEquals('MyTitle', $title);
        $text=$obj->{'text'};
        $this->assertEquals('ItisText', $text);


    }

}
