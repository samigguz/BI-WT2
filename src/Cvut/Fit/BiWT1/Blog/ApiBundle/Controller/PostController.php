<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/1/15
 * Time: 11:56 AM
 */

namespace Cvut\Fit\BiWT1\Blog\ApiBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
//use FOS\RestBundle\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;




/**
 * Class PostController
 * @package Cvut\Fit\BiWT1\Blog\ApiBundle\Controller
 */
class PostController extends FOSRestController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @View(serializerGroups={"Post"})
     * @Route("/api/get_posts_all", name="get_posts_all")
     *
     * @ApiDoc(
     *  description="Get All Posts",
     *  output="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post"
     * )
     */
    public function getAllPosts()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts = $tmp->findAll();

        $count=$posts->count();
        $templateData = array('count' => $count);

        $view = $this->view($posts, 200)
            ->setTemplate("CvutFitBiWT1BlogUiBundle:Post:show.html.twig")
            ->setTemplateVar('posts')
            ->setTemplateData($templateData)
        ;
        return $this->handleView($view);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @View(serializerGroups={"Post"})
     * @Route("/api/get_post/{id}.{_format}", name="get_post", requirements={"_format"="json|xml"})
     *
     *  @ApiDoc(
     *  description="Get one post by id",
     *  output="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post",
     * )
     */
    public function getOnePost($id)
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $tmp->findById($id);

        if (!$post instanceof Post) {
            throw new NotFoundHttpException('Post not found');
        }

        $view = $this->view($post, 200);

       return $this->handleView($view);

    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @View(serializerGroups={"Post"})
     * @Route("/api/get_one/{id}", name="get_one")
     *
     */
    public function getOne($id)
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $tmp->findById($id);

        if (!$post instanceof Post) {
            throw new NotFoundHttpException('Post not found');
        }

        $view = $this->view($post, 200)
        ->setFormat('json');

        return $this->handleView($view);

    }

    /**
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @View(serializerGroups={"Post"})
     * @Route("/api/all.{_format}", name="get_all", requirements={"_format"="json|xml"})
     *
     *  @ApiDoc(
     *  description="Get one post by id",
     *  output="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post",
     * )
     */
    public function getAllPost()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts = $tmp->findAll();
        $view = $this->view($posts, 200);

        return $this->handleView($view);

    }




}
