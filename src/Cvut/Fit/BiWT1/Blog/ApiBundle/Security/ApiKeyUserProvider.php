<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 07.04.2016
 * Time: 12:00
 */

namespace Cvut\Fit\BiWT1\Blog\ApiBundle\Security;


use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\UserRepository;
class ApiKeyUserProvider implements UserProviderInterface
{

    protected $userRepository;

    public function __construct(
        UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

    }

    public function getUsernameForApiKey($apiKey)
    {
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
       // $tmp = $this->get('cvut_fit_biwt1_blog_base.service.functionality.user');
       // $username = $tmp->findUsernameByApiKey($apiKey);
        $username=$this->userRepository->findUsernameByApiKey($apiKey);
        return $username;
    }
    public function getUserForApiKey($apiKey)
    {
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        //$tmp = $this->getC('cvut_fit_biwt1_blog_base.service.functionality.user');


        $user = $this->userRepository->findByApiKey($apiKey);
        return $user;
    }
    public function loadUserByUsername($username)
    {
        return new User(
            $username,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            array('ROLE_USER')
        );
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        //throw new UnsupportedUserException();
        return $user;
    }

    public function supportsClass($class)
    {
        //return 'Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\User' === $class;
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}