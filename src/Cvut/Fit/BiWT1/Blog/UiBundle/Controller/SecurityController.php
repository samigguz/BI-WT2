<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/1/15
 * Time: 11:56 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\RegistrationType;


class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {

        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();


       /*$user = new User();
        $plainPassword = 'ryanpass';
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($user, $plainPassword);
        $user->setPassword($encoded);*/


        return $this->render(
            'CvutFitBiWT1BlogUiBundle:Post:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );

    }
    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        $this->container->get('security.context')->setToken(null);

        return $this->redirect($this->generateUrl('login_route'));
    }





}
