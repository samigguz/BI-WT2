<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/1/15
 * Time: 11:56 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Controller;


use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\PostType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\UpdatePostType;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\UpdateCommentType;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Comment;
use Cvut\Fit\BiWT1\Blog\UiBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Security\Voter;
use Cvut\Fit\BiWT1\Blog\BaseBundle\Security\Voter\PostVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class PostController extends Controller
{

    /**
     * @Route("/file", name="file")
     * @Template()
     *
     */
    public function fileAction(){
        
        $post = new File();
        $form = $this->createForm(new PostType(), $post);
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findAll();
        $tags=$tmp->getTags();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:tmp.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'tag'=>"Life"
        ));
    }
    
    /**
     * @Route("/tmp", name="tmp")
     * @Template()
     *
     */
    public function tmpAction(){

        //return $this->render('CvutFitBiWT1BlogUiBundle:Post:tmp.html.twig');
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findAll();
        $tags=$tmp->getTags();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:tmp.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'tag'=>"Life"
        ));
    }
    /**
     * @Route("/angular", name="angular")
     * @Template()
     *
     */
    public function angularAction(){
        return $this->render('CvutFitBiWT1BlogUiBundle:Angular:index.html.twig');}



    /**
     * @Route("/angular_detail/{id}", name="angular_detail")
     * @Template()
     *
     */
    public function angularDetailAction($id){
        return $this->render('CvutFitBiWT1BlogUiBundle:Angular:detail.html.twig');}


    /**
     * @Route("/tmp_detail/{id}", name="tmp_detail")
     * @Template()
     *
     */
    public function tmpDetailAction(Request $request,$id){

        //return $this->render('CvutFitBiWT1BlogUiBundle:Post:tmp.html.twig');
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post=$tmp->findById($id);
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:tmp_detail.html.twig',array(
            'post' => $post
        ));
    }

    /**
     * @Route("/new", name="new")
     * @Template()
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *  description="Create a new Post",
     *  input="Cvut\Fit\BiWT1\Blog\UiBundle\Form\PostType",
     *  output="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post"
     * )
     */


    public function newAction(Request $request)
{
    $post = new Post();
    $form = $this->createForm(new PostType(), $post);
    $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
    $tags=$tmp->getTags();
    $post->setModified(new \DateTime());
    $post->setAuthor($this->getUser());
    $form->handleRequest($request);

    

    if($form->isSubmitted())
    {


        if ($post->getpublishFrom() > $post->getPublishTo())
        {
            $form->addError(new FormError('Error in publishTo.'));
        }


        if($form->isValid())
        {

            $file = $post->getFile();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $fileDir = $this->container->getParameter('kernel.root_dir').'/../web/images';
            $file->move($fileDir, $fileName);
            $post->setFile($fileName);

            $tmp->create($post,array(),array());

            //
            //
        } else{

        }

        return $this->redirectToRoute($post->getTag());
        //return $this->redirectToRoute('/post/'.$post->getId().'/detail');
    }


    return $this->render('CvutFitBiWT1BlogUiBundle:Post:new.html.twig', array(
        'form' => $form->createView(),
        'tags'=>$tags
    ));
}

    /**
     * @ApiDoc(
     *  description="Detail of Post",
     *  input="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post",
     *  output="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post"
     * )
     * @Route("/post/{id}/detail", name="detail")
     */
    public function detailAction($id,Request $request)
    {

        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post=$tmp->findById($id);
        $tags=$tmp->getTags();

        $comment = new Comment();

        $this->denyAccessUnlessGranted('view',$post);

        if ($this->getUser()) {

            $form = $this->createForm(new CommentType(), $comment);
            $comment->setModified(new \DateTime());
            $comment->setCreated(new \DateTime());
            $comment->setPost($post);
            $comment->setAuthorName($this->getUser()->getUsername());
            $comment->setAuthor($this->getUser());
            $form->handleRequest($request);
            if ($form->isSubmitted()) {

                if ($form->isValid()) {

                    $tmp->createComment($comment);

                    return $this->redirectToRoute('detail', array('id' => $id));
                }
            }

            $operation = $this->get('cvut_fit_biwt1_blog_base.service.operation.comment');
            $comments = $operation->getByPost(1, $post);
            $limit = 4;
            $maxPages = ceil($comments->count() / $limit);
            return $this->render('CvutFitBiWT1BlogUiBundle:Post:detail.html.twig', array(
                'post' => $post,
                'tags' => $tags,
                'form' => $form->createView(),
                'month' => 0, 'year' => 0,
                'comments' => $comments, 'maxPages' => $maxPages, 'thisPage' => 1, 'replyy' => '0'
            ));
        }
        else{
            return $this->render('CvutFitBiWT1BlogUiBundle:Post:detail.html.twig', array(
                'post' => $post,
                'tags' => $tags,
                'month' => 0, 'year' => 0
            ));
        }
    }
    /**
     * @Route("/update/{id}", name="update")
     * @Template()
     *
     * @ApiDoc(
     *  description="Update the Post",
     *  input="Cvut\Fit\BiWT1\Blog\UiBundle\Form\PostType",
     *  output="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post"
     * )
     */
    public function updateAction($id,Request $request)
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post =$tmp->findById($id);
        $form = $this->createForm(new UpdatePostType(), $post);

        $tags=$tmp->getTags();
        $post->setModified(new \DateTime());

       $form->handleRequest($request);

        $this->denyAccessUnlessGranted('edit', $post);


        if($form->isSubmitted())
        {


            if ($post->getpublishFrom() > $post->getPublishTo())
            {
                $form->addError(new FormError('Error in publishTo.'));
            }


            if($form->isValid())
            {
                $tmp->update($post,array(),array());

                return $this->redirectToRoute('detail', array('id'=>$post->getId()));
            } else{

            }
        }


        return $this->render('CvutFitBiWT1BlogUiBundle:Post:new.html.twig', array(
            'form' => $form->createView(),
            'tags'=>$tags
        ));
    }

    /**
     * @Route("/updateComment/{id}", name="updateComment")
     * @Template()
     * @Security("is_granted('ROLE_ADMIN')")
     *
     *
     * @ApiDoc(
     *  description="Update the Comment",
     *  input="Cvut\Fit\BiWT1\Blog\UiBundle\Form\CommentType",
     *  output="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Comment"
     * )
     */
    public function updateCommentAction($id,Request $request)
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.comment');
        $tmp2= $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $comment = $tmp->getById($id);
        $form = $this->createForm(new UpdateCommentType(), $comment);
        $tags=$tmp2->getTags();
        $comment->setModified(new \DateTime());



        $post =$tmp2->findPostByCommentId($id);
        $form->handleRequest($request);

        if($form->isSubmitted())
        {



            if($form->isValid())
            {
                $tmp2->updateComment($comment);
                return $this->redirectToRoute('detail', array('id'=>$post));
            } else{

            }
        }

        return $this->render('CvutFitBiWT1BlogUiBundle:Post:comment.html.twig', array(
            'form' => $form->createView(),
            'tags'=>$tags
        ));
    }
    /**
     * @Route("/author", name="author")
     * @Template()
     */
    public function authorAction(Request $request)
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $tags=$tmp->getTags();
        $users=$tmp->getAllUsers();
        $posts=$tmp->findAll();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:author_filtering.html.twig', array(
           // 'form' => $form->createView(),
            'users'=>$users,
            'tags'=>$tags,
            'posts'=>$posts
        ));

    }
    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findAll();
        $tags=$tmp->getTags();

       return $this->render('CvutFitBiWT1BlogUiBundle:Post:index.html.twig',array(
           'posts' => $posts,
           'tags'=>$tags
       ));


    //return $this->render(...);
    }
    /**
     * @Route("/politics", name="politics")
     */
    public function politicsAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findByTitle("Politics");
        $tags=$tmp->getTags();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:politics.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'tag'=>"Politics"
        ));
    }
    /**
     * @Route("/trending", name="trending")
     */
    public function trendingAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findByTitle("Trending");
        $tags=$tmp->getTags();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:politics.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'tag'=>"Trending"
        ));
    }
    /**
     * @Route("/tech", name="tech")
     */
    public function techAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findByTitle("Tech");
        $tags=$tmp->getTags();
        $comments=$tmp->getComments();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:politics.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'comments'=>$comments,
            'tag'=>"Tech"
        ));
    }
    /**
     * @Route("/sport", name="sport")
     */
    public function sportAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findByTitle("Sport");
        $tags=$tmp->getTags();

        return $this->render('CvutFitBiWT1BlogUiBundle:Post:politics.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'tag'=>"Sport",

        ));
    }
    /**
     * @Route("/life", name="life")
     */
    public function lifeAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findByTitle("Life");
        $tags=$tmp->getTags();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:politics.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'tag'=>"Life"
        ));
    }
    /**
     * @Route("/All", name="All")
     */
    public function allAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findAll();
        $tags=$tmp->getTags();
        $count=$posts->count();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:all.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'count'=>$count,
            'startPage'=>1
        ));

    }
    /**
     * @Route("/getByAuthor", name="getByAuthor")
     */
    public function getByAuthorAction($author)
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $posts=$tmp->findByAuthor($author);
        $tags=$tmp->getTags();
        $count=$tmp->getCount();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:all.html.twig',array(
            'posts' => $posts,
            'tags'=>$tags,
            'count'=>$count,
            'startPage'=>1
        ));

    }


    /**
     * @Route("/post/{id}/delete", name="delete")
     *
     *
     * @ApiDoc(
     *  description="Delete the Post",
     *  input="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Post"
     * )
     */
    public function deleteAction($id)
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');

        $post=$tmp->findById($id);

        $this->denyAccessUnlessGranted('edit', $post);

        $tmp->removePost($post);
        return $this->redirectToRoute('All');
    }
    /**
     * @Route("/comment/{id}/delete", name="deleteComment")
     * @Security("is_granted('ROLE_ADMIN')")
     *
     *  @ApiDoc(
     *  description="Delete the Comment",
     *  input="Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Comment"
     * )
     */
    public function deleteComment($id)
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $comment=$tmp->findByCommentId($id);
        $postId=$comment->getPost()->getId();
        $tmp->removeComment($comment);
        return $this->redirectToRoute('detail', array('id' => $postId));
    }
    /**
     * @Route("/post/{id}/detail/page/{page}", name="detail_page")
     */
    public function detailPageAction(Request $request, $id, $page)
    {
        $operation = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $post = $operation->findById($id);
        $tags = $operation->getTags();
        //$authors = $operation->getAllUsers();
        $comment = new Comment();
        $form = $this->createForm(new CommentType(), $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $operation = $this->get('cvut_fit_biwt1_blog_base.service.operation.comment');
                $comment->setPost($post);
                $comment->setCreated(new \DateTime());
                $operation->create($comment);
                return $this->redirectToRoute('detail', array('id' => $id));
            }
        }
        $count=$post->getComments()->count();
        $operation = $this->get('cvut_fit_biwt1_blog_base.service.operation.comment');
        $comments = $operation->getByPost($page, $post);
        $limit = 4;
        $maxPages = ceil($comments->count() / $limit);
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:detail.html.twig',
            array('post' => $post, 'tags' => $tags, 'form' => $form->createView(),
                'month' => 0, 'year' => 0, 'comments' => $comments, 'maxPages' => $maxPages, 'thisPage' => $page, 'replyy' => '0', 'count'=>$count,));
    }

    /**
     * @Route("/date", name="date")
     * @Template()
     */
    public function dateAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $tags=$tmp->getTags();
        $posts=$tmp->findAll();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:date_filtering.html.twig', array(

            'posts'=>$posts,
            'tags'=>$tags
        ));

    }
    /**
     * @Route("/tag", name="tag")
     * @Template()
     */
    public function tagAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $tags=$tmp->getTags();
        $posts=$tmp->findAll();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:tag_filtering.html.twig', array(
            'tags'=>$tags,
             'posts'=>$posts
        ));

    }
    /**
     * @Route("/private", name="private")
     * @Template()
     */
    public function privateAction()
    {
        $tmp = $this->get('cvut_fit_biwt1_blog_base.service.operation.post');
        $tags=$tmp->getTags();
        $posts=$tmp->findAll();
        return $this->render('CvutFitBiWT1BlogUiBundle:Post:private_filtering.html.twig', array(
            'tags'=>$tags,
            'posts'=>$posts
        ));

    }

}
