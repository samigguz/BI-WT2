<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/8/15
 * Time: 10:45 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder

            ->add('text','textarea',array('label' => 'Add a comment...'))
            ->add('save','submit', array('label' => 'Create'))
        ;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'form_title';
    }
}