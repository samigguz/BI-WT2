<?php
/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 12/8/15
 * Time: 10:45 AM
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', 'text', array('label' => 'Title'))
            ->add('text','textarea',array('label' => 'Text'))
            ->add('private','checkbox',array('required'=>false))
            ->add('publishFrom','date',array(
                'format' => 'dd-MM-yyyy'))
            ->add('publishTo','date',array(
                'format' => 'dd-MM-yyyy'))
            ->add('tags','entity', array(
                'class' => 'Cvut\Fit\BiWT1\Blog\BaseBundle\Entity\Tag',
                'property' => 'title',
                'expanded' => true,
                'multiple' => true,
            ))
            //->add('file', 'file')
             // ->add('')
            ->add('files', 'collection', array(
                    'type' => new FileType(),
                    'allow_add' => true,
                    'by_reference' => false,
                    'prototype' => true,
                    'label' => "file",
                    'allow_delete' => true)
            )
            ->add('save','submit', array('label' => 'Create'))
        ;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'form_post';
    }
}