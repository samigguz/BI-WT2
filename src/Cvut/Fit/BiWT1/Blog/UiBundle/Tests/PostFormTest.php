<?php
/**
 * Created by PhpStorm.
 * User: Guzel
 * Date: 10.03.2016
 * Time: 23:33
 */

namespace Cvut\Fit\BiWT1\Blog\UiBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Cookie;


class PostFormTest extends WebTestCase
{
    /** @var Client */
    protected $client;

    public function setUp()
    {
        parent::setUp();
        $this->client = static::createClient([], [
            'HTTP_HOST' => 'localhost:8000'
        ]);
    }


    public function testLogin()
    {
        // request
        $crawler = $this->client->request('GET', '/login');


        $form = $crawler->selectButton('login')->form();
        $crawler = $this->client->submit($form, array('_username' => 'Guzel',
        '_password'=>'guzel'
        ));
        $crawler = $this->client->followRedirect();
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        /*$form = $crawler->selectButton('submit')->form(array(
            '_username'  => 'Guzel',
            '_password'  => 'guzel',
        ));

        $this->client->submit($form);*/


        //$crawler = $this->client->followRedirect();

        return $this->client;
    }
    // ...
    /**
     * @depends testLogin
     */
    public function testForm($loggedClient = null)
    {
        $this->client = $loggedClient;

        $crawler = $this->client->request('GET', '/new');
        $this->assertTrue($this->client->getResponse()->isSuccessful());



        $form_post = $crawler->filter('button[type=submit]')->last()->form();


        $form_post['form_post[title]'] = 'My Title';
        $form_post['form_post[text]'] = 'ItisText';
        $form_post['form_post[tags]'][1]->tick();



        $crawler = $this->client->submit($form_post);
        $crawler = $this->client->followRedirect();

        $this->assertTrue($this->client->getResponse()->isSuccessful());
        //var_dump($this->client->getResponse()->getContent());


        $this->assertGreaterThan(1, $crawler->filter('p:contains("My Title")')->count());


        $crawler = $this->client->request('GET', '/tech');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        var_dump($this->client->getResponse()->getContent());

        $title = $crawler->filter('a.title')->last()->text();
        $this->assertEquals('My Title', $title);


    }


 /*   public function testSecuredHello()
    {
        $this->logIn();

        $crawler = $this->client->request('GET', '/new');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $form = $crawler->selectButton('submit')->form();

        // set some values
        $form['title'] = 'Title 1';
        $form['text'] = 'text';
        $form['tags']->select('Sport');

        // submit the form
        $crawler = $this->client->submit($form);

        $this->assertTrue(
            $this->client->getResponse()->isRedirect('^/post')
        );


        $this->assertGreaterThan(1, $crawler->filter('p.title')->count());
        $this->assertGreaterThan(1, $crawler->filter('p.text')->count());
        $this->assertGreaterThan(1, $crawler->filter('p:contains("Title")')->count());
        $this->assertGreaterThan(1, $crawler->filter('p:contains("text")')->count());

        $crawler = $this->client->request('GET', '/sport');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertGreaterThan(1, $crawler->filter('p.title:contains("Title")')->count());


    }*/

   /* private function logIn()
    {
        $session = $this->client->getContainer()->get('session');

        $firewall = 'secured_area';
        $token = new UsernamePasswordToken('admin', null, $firewall, array('ROLE_ADMIN'));
        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }*/

}
